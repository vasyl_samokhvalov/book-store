﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace BookStore.BusinessLogic
{
    public class AuthenticationOptions
    {
        public const string ISSUER = "MyAuthServer";
        public const string AUDIENCE = "http://localhost:44367/";
        const string KEY = "mysupersecret_secretkey!123";
        public const int ACCESS_TOKEN_LIFETIME = 30;
        public const int REFRESH_TOKEN_LIFETIME = 5;

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
