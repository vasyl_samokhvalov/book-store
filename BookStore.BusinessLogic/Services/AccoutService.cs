﻿using BookStore.BusinessLogic.Views;
using BookStore.DataAccess.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BookStore.BusinessLogic.Services
{
    public class AccountService : IAccountService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public AccountService(SignInManager<User> signInManager, UserManager<User> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }

        public async Task<string> Login([FromBody]LoginUserView model)
        {
            User user = await _userManager.FindByEmailAsync(model.Email);

            if (user == null)
            {
                return "We didn't find any account with this name, try to register!";
            }

            if (!await _userManager.IsEmailConfirmedAsync(user))
            {
                return "You have not confirmed your e-mail.";
            }
            var result = new Microsoft.AspNetCore.Identity.SignInResult();

            result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, true, false);

            var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role)
                };
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            if (await _userManager.GetLockoutEndDateAsync(user) >= DateTime.UtcNow)
            {
                return $"Your account has been blocked due to a large number of password input attempts. Try again after {await _userManager.GetLockoutEndDateAsync(user)}.";
            }
            if (!result.Succeeded)
            {
                await _userManager.AccessFailedAsync(user);
            }

            if (await _userManager.GetAccessFailedCountAsync(user) >= 3)
            {
                await _userManager.ResetAccessFailedCountAsync(user);
                int blockTime = 10;//in minutes
                await _userManager.SetLockoutEndDateAsync(user, DateTime.UtcNow.AddMinutes(blockTime));
                return $"Your account has been blocked due to a large number of password input attempts. Try again after {blockTime} minutes.";
            }

            await _userManager.ResetAccessFailedCountAsync(user);
            TokenView tokenViewModel = CreateTokens(user.Email, user.UserName, user.Id, claimsIdentity);
            tokenViewModel.Role = user.Role;
            return JsonConvert.SerializeObject(tokenViewModel, new JsonSerializerSettings { Formatting = Formatting.Indented });
        }

        public async void Logout()
        {
            await _signInManager.SignOutAsync();
        }

        public TokenView CreateTokens([FromBody]string Email, string UserName, string Id, ClaimsIdentity claimsIdentity)
        {
            var now = DateTime.UtcNow;

            Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1969, 12, 1))).TotalSeconds;

            TokenView tokenViewModel = new TokenView
            {
                AccessToken = CreateAccessToken(claimsIdentity),
                RefreshToken = CreateRefreshToken(claimsIdentity),
                UserEmail = Email,
                UserName = UserName,
                UserId = Id
            };

            return tokenViewModel;
        }

        public string CreateAccessToken([FromBody]ClaimsIdentity claimsIdentity)
        {
            var now = DateTime.UtcNow;

            var jwt = new JwtSecurityToken(
                    issuer: AuthenticationOptions.ISSUER,
                    audience: AuthenticationOptions.AUDIENCE,
                    notBefore: now,
                    claims: claimsIdentity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthenticationOptions.ACCESS_TOKEN_LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthenticationOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;
        }

        public string CreateRefreshToken([FromBody]ClaimsIdentity claimsIdentity)
        {
            var now = DateTime.UtcNow;

            var refreshjwt = new JwtSecurityToken(
                issuer: AuthenticationOptions.ISSUER,
                audience: AuthenticationOptions.AUDIENCE,
                notBefore: now,
                claims: claimsIdentity.Claims,
                expires: now.Add(TimeSpan.FromDays(AuthenticationOptions.REFRESH_TOKEN_LIFETIME)),
                signingCredentials: new SigningCredentials(AuthenticationOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedRefreshJwt = new JwtSecurityTokenHandler().WriteToken(refreshjwt);
            return encodedRefreshJwt;
        }

        public async Task<TokenView> CheckTokens(TokenView token)
        {

            var decodedAccessJwt = new JwtSecurityTokenHandler().ReadJwtToken(token.AccessToken);
            var decodedRefreshJwt = new JwtSecurityTokenHandler().ReadJwtToken(token.RefreshToken);

            User temp = await _userManager.FindByEmailAsync(token.UserEmail);
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, temp.Email),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, temp.Role)
            };
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            if (decodedAccessJwt == null || (decodedRefreshJwt.ValidTo != null && decodedRefreshJwt.ValidTo < DateTime.UtcNow))
            {
                return CreateTokens(token.UserEmail, token.UserName, token.UserId, claimsIdentity);
            }
            if (decodedAccessJwt.ValidTo < DateTime.UtcNow)
            {
                token.AccessToken = CreateAccessToken(claimsIdentity);
            }
            return token;
        }

        public async Task<string> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return "Error";
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return "Error";
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);
            if (!result.Succeeded)
            {
                return "Error";
            }
            return "Ok";
        }

        public Task<string> GenerateEmailConfirmationTokenAsync(RegisterView item)
        {
            User user = new User { Email = item.Email, UserName = item.Email };
            return _userManager.GenerateEmailConfirmationTokenAsync(user);
        }
    }
}
