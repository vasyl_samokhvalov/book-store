﻿using AutoMapper;
using BookStore.BusinessLogic.Views;
using BookStore.DataAccess.Entities;
using System.Collections.Generic;
using BookStore.DataAccess.RepositoryInterfaces;

namespace BookStore.BusinessLogic.Services
{
    public class PublisherService : IPublisherService
    {
        private readonly IPublisherRepository _publisherRepository;
        private readonly IMapper _mapper;

        public PublisherService(IPublisherRepository publisherRepository, IMapper mapper)
        {
            _publisherRepository = publisherRepository;
            _mapper = mapper;
        }

        public PublisherService()
        {
        }

        public GetAllPublishersView GetAll()
        {
            List<Publisher> Publishers = _publisherRepository.GetAll();
            var publishersView = new GetAllPublishersView
            {
                Publishers = _mapper.Map<List<Publisher>, List<GetAllPublishersViewItem>>(Publishers)
            };
            return publishersView;
        }

        public long Create(CreatePublisherView item)
        {
            Publisher publisher = new Publisher();
            publisher = _mapper.Map<CreatePublisherView, Publisher>(item);
            return _publisherRepository.Create(publisher);
        }

        public void Update(UpdatePublisherView item)
        {
            Publisher publisher = new Publisher();
            publisher = _mapper.Map<UpdatePublisherView, Publisher>(item);

            _publisherRepository.Update(publisher);
        }

        public void Delete(int id)
        {
            _publisherRepository.Delete(id);
        }
    }
}
