﻿using AutoMapper;
using BookStore.BusinessLogic.Views;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.RepositoryInterfaces;
using System.Collections.Generic;

namespace BookStore.BusinessLogic.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;

        public OrderService(IOrderRepository orderRepository, IMapper mapper)
        {
            _orderRepository = orderRepository;
            _mapper = mapper;
        }

        public OrderService()
        {
        }

        public GetAllOrdersView GetAll()
        {
            List<Order> Orders = _orderRepository.GetAll();
            var ordersView = new GetAllOrdersView
            {
                Orders = _mapper.Map<List<Order>, List<GetAllOrdersViewItem>>(Orders)
            };
            return ordersView;
        }

        public long Create(CreateOrderView item)
        {
            Order order = new Order();
            order = _mapper.Map<CreateOrderView, Order>(item);
            return _orderRepository.Create(order);
        }

        public void Update(UpdateOrderView item)
        {
            Order order = new Order();
            order = _mapper.Map<UpdateOrderView, Order>(item);

            _orderRepository.Update(order);
        }

        public void Delete(int id)
        {
            _orderRepository.Delete(id);
        }
        public GetUserOrderView GetUserOrder(string userId)
        {
            List<UserOrder> orders = _orderRepository.GetUserOrder(userId);
            var ordersView = new GetUserOrderView();
            ordersView.Orders = _mapper.Map<List<UserOrder>, List<GetUserOrderViewItem>>(orders);
            return ordersView;
        }
    }
}
