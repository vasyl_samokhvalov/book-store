﻿using AutoMapper;
using BookStore.BusinessLogic.Views;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.RepositoryInterfaces;
using System.Collections.Generic;

namespace BookStore.BusinessLogic.Services
{
    public class AuthorService : IAuthorService
    {
        private readonly IAuthorRepository _authorRepository;
        private readonly IMapper _mapper;

        public AuthorService(IAuthorRepository authorRepository, IMapper mapper)
        {
            _authorRepository = authorRepository;
            _mapper = mapper;
        }

        public AuthorService()
        {
        }

        public GetAllAuthorsView GetAll()
        {
            List<Author> Authors = _authorRepository.GetAll();
            var authorsView = new GetAllAuthorsView
            {
                Authors = _mapper.Map<List<Author>, List<GetAllAuthorsViewItem>>(Authors)
            };
            return authorsView;
        }

        public long Create(CreateAuthorView item)
        {
            Author author = new Author();
            author = _mapper.Map<CreateAuthorView, Author>(item);
            return _authorRepository.Create(author);
        }

        public void Update(UpdateAuthorView item)
        {
            Author author = new Author();
            author = _mapper.Map<UpdateAuthorView, Author>(item);

            _authorRepository.Update(author);
        }

        public void Delete(int id)
        {
            _authorRepository.Delete(id);
        }
    }
}
