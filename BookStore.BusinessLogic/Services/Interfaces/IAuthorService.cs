﻿using BookStore.BusinessLogic.Views;

namespace BookStore.BusinessLogic.Services
{
    public interface IAuthorService
    {
        GetAllAuthorsView GetAll();
        long Create(CreateAuthorView item);
        void Update(UpdateAuthorView item);
        void Delete(int id);
    }
}
