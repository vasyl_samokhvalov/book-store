﻿using BookStore.BusinessLogic.Views;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace BookStore.BusinessLogic.Services
{
    public interface IUserService
    {
        GetAllUsersView GetAll();
        Task<IdentityResult> Create(RegisterView item);
        Task<string> GetUserId(RegisterView item);
    }
}
