﻿using BookStore.BusinessLogic.Views;

namespace BookStore.BusinessLogic.Services
{
    public interface IJournalService
    {
        GetAllJournalsView GetAll();
        long Create(CreateJournalView item);
        void Update(UpdateJournalView item);
        void Delete(int id);
    }
}
