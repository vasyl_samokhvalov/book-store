﻿using BookStore.BusinessLogic.Views;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BookStore.BusinessLogic.Services
{
    public interface IAccountService
    {
        Task<string> Login(LoginUserView item);
        TokenView CreateTokens(string Email, string UserName, string Id, ClaimsIdentity claimsIdentity);
        string CreateAccessToken(ClaimsIdentity claimsIdentity);
        string CreateRefreshToken(ClaimsIdentity claimsIdentity);
        Task<TokenView> CheckTokens(TokenView token);
        void Logout();
        Task<string> ConfirmEmail(string userId, string code);
        Task<string> GenerateEmailConfirmationTokenAsync(RegisterView item);
    }
}
