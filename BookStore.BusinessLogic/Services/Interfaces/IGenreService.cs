﻿using BookStore.BusinessLogic.Views;

namespace BookStore.BusinessLogic.Services
{
    public interface IGenreService
    {
        GetAllGenresView GetAll();
        long Create(CreateGenreView item);
        void Update(UpdateGenreView item);
        void Delete(int id);
    }
}
