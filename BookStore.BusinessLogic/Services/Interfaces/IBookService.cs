﻿using BookStore.BusinessLogic.Views;
using System.Threading.Tasks;

namespace BookStore.BusinessLogic.Services
{
    public interface IBookService
    {
        Task<GetAllBooksView> GetAll();
        Task<int> Create(CreateBookView item);
        void Update(UpdateBookView item);
        void Delete(int id);
    }
}
