﻿using BookStore.BusinessLogic.Views;

namespace BookStore.BusinessLogic.Services
{
    public interface IOrderService
    {
        GetAllOrdersView GetAll();
        long Create(CreateOrderView item);
        void Update(UpdateOrderView item);
        void Delete(int id);
        GetUserOrderView GetUserOrder(string userId);
    }
}
