﻿using BookStore.BusinessLogic.Views;

namespace BookStore.BusinessLogic.Services
{
    public interface IPublisherService
    {
        GetAllPublishersView GetAll();
        long Create(CreatePublisherView item);
        void Update(UpdatePublisherView item);
        void Delete(int id);
    }
}
