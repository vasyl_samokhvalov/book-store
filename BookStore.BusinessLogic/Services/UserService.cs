﻿using AutoMapper;
using BookStore.BusinessLogic.Views;
using BookStore.DataAccess.Entities;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.BusinessLogic.Services
{
    public class UserService : IUserService
    {
        UserManager<User> _userManager;
        private readonly IMapper _mapper;

        public UserService(IMapper mapper, UserManager<User> userManager)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        public GetAllUsersView GetAll()
        {
            List<User> users = _userManager.Users.ToList();
            var usersView = new GetAllUsersView();
            usersView.Users = _mapper.Map<List<User>, List<GetAllUsersViewItem>>(users);

            return usersView;
        }

        public async Task<IdentityResult> Create(RegisterView item)
        {
            User user = new User { Email = item.Email, UserName = item.Email };
            var result = await _userManager.CreateAsync(user, item.Password);
            return result;
        }

        public Task<string> GetUserId(RegisterView item)
        {
            User user = new User { Email = item.Email, UserName = item.Email };
            return _userManager.GetUserIdAsync(user);
        }
    }
}
