﻿using AutoMapper;
using BookStore.BusinessLogic.Views;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.RepositoryInterfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.BusinessLogic.Services
{
    public class BookService : IBookService
    {
        private readonly IBookRepository _bookRepository;
        private readonly IBookAuthorRepository _bookAuthorRepository;
        private readonly IMapper _mapper;

        public BookService(IBookRepository repository, IMapper mapper, IBookAuthorRepository bookAuthorRepository)
        {
            _bookRepository = repository;
            _mapper = mapper;
            _bookAuthorRepository = bookAuthorRepository;
        }

        public BookService()
        {

        }

        public async Task<GetAllBooksView> GetAll()
        {
            List<Book> Books = await _bookRepository.GetAll();
            List<BookAuthor> BA = _bookAuthorRepository.GetAll();
            var booksView = new GetAllBooksView
            {
                Books = _mapper.Map<List<Book>, List<GetAllBooksViewItem>>(Books)
            };
            foreach (BookAuthor bookAuthor in BA)
            {
                booksView.Books[bookAuthor.BookId - 1].Authors.Add(bookAuthor.AuthorId);
            }
            return booksView;
        }

        public async Task<int> Create(CreateBookView item)
        {
            Book book = new Book();
            book = _mapper.Map<CreateBookView, Book>(item);
            return await _bookRepository.Create(book);
        }

        public void Update(UpdateBookView item)
        {
            Book book = new Book();
            book = _mapper.Map<UpdateBookView, Book>(item);

            _bookRepository.Update(book);
        }

        public void Delete(int id)
        {
            _bookRepository.Delete(id);
        }

        public GetAllBooksView GetBasket(List<int> bookId)
        {
            List<Book> Books = _bookRepository.GetBasket(bookId);
            var booksView = new GetAllBooksView
            {
                Books = _mapper.Map<List<Book>, List<GetAllBooksViewItem>>(Books)
            };
            return booksView;
        }
    }
}
