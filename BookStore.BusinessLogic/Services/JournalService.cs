﻿using AutoMapper;
using BookStore.BusinessLogic.Views;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.RepositoryInterfaces;
using System.Collections.Generic;

namespace BookStore.BusinessLogic.Services
{
    public class JournalService : IJournalService
    {
        private readonly IJournalRepository _journalRepository;
        private readonly IMapper _mapper;

        public JournalService(IJournalRepository journalRepository, IMapper mapper)
        {
            _journalRepository = journalRepository;
            _mapper = mapper;
        }

        public JournalService()
        {
        }

        public GetAllJournalsView GetAll()
        {
            List<Journal> Journals = _journalRepository.GetAll();
            var journalsView = new GetAllJournalsView
            {
                Journals = _mapper.Map<List<Journal>, List<GetAllJournalsViewItem>>(Journals)
            };
            return journalsView;
        }

        public long Create(CreateJournalView item)
        {
            Journal journal = new Journal();
            journal = _mapper.Map<CreateJournalView, Journal>(item);
            return _journalRepository.Create(journal);
        }

        public void Update(UpdateJournalView item)
        {
            Journal journal = new Journal();
            journal = _mapper.Map<UpdateJournalView, Journal>(item);

            _journalRepository.Update(journal);
        }

        public void Delete(int id)
        {
            _journalRepository.Delete(id);
        }
    }
}
