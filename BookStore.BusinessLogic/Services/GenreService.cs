﻿using AutoMapper;
using BookStore.BusinessLogic.Views;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.RepositoryInterfaces;
using System.Collections.Generic;

namespace BookStore.BusinessLogic.Services
{
    public class GenreService : IGenreService
    {
        private readonly IGenreRepository _genreRepository;
        private readonly IMapper _mapper;

        public GenreService(IGenreRepository genreRepository, IMapper mapper)
        {
            _genreRepository = genreRepository;
            _mapper = mapper;
        }

        public GenreService()
        {
        }

        public GetAllGenresView GetAll()
        {
            List<Genre> Genres = _genreRepository.GetAll();
            var genresView = new GetAllGenresView
            {
                Genres = _mapper.Map<List<Genre>, List<GetAllGenresViewItem>>(Genres)
            };
            return genresView;
        }

        public long Create(CreateGenreView item)
        {
            Genre genre = new Genre();
            genre = _mapper.Map<CreateGenreView, Genre>(item);
            return _genreRepository.Create(genre);
        }

        public void Update(UpdateGenreView item)
        {
            Genre genre = new Genre();
            genre = _mapper.Map<UpdateGenreView, Genre>(item);

            _genreRepository.Update(genre);
        }

        public void Delete(int id)
        {
            _genreRepository.Delete(id);
        }
    }
}
