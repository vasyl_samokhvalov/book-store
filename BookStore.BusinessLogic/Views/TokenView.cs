﻿namespace BookStore.BusinessLogic.Views
{
    public class TokenView
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string UserId { get; set; }
        public string Role { get; set; }
    }
}
