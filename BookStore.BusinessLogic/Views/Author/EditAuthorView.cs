﻿namespace BookStore.BusinessLogic.Views
{
    public class UpdateAuthorView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
