﻿using System.Collections.Generic;

namespace BookStore.BusinessLogic.Views
{
    public class GetAllAuthorsView
    {
        public List<GetAllAuthorsViewItem> Authors { get; set; }

        public GetAllAuthorsView()
        {
            Authors = new List<GetAllAuthorsViewItem>();
        }
    }

    public class GetAllAuthorsViewItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
