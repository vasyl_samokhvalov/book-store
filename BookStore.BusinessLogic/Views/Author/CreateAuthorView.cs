﻿namespace BookStore.BusinessLogic.Views
{
    public class CreateAuthorView
    {
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
