﻿namespace BookStore.BusinessLogic.Views
{
    public class CreatePublisherView
    {
        public string Name { get; set; }
    }
}
