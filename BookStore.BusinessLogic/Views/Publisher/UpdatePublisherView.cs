﻿namespace BookStore.BusinessLogic.Views
{
    public class UpdatePublisherView
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
