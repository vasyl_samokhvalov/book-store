﻿using System.Collections.Generic;

namespace BookStore.BusinessLogic.Views
{
    public class GetAllPublishersView
    {
        public List<GetAllPublishersViewItem> Publishers { get; set; }

        public GetAllPublishersView()
        {
            Publishers = new List<GetAllPublishersViewItem>();
        }
    }

    public class GetAllPublishersViewItem
    {
        public int Id;
        public string Name;
    }
}
