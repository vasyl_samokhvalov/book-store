﻿using System.Collections.Generic;

namespace BookStore.BusinessLogic.Views
{
    public class GetAllJournalsView
    {
        public List<GetAllJournalsViewItem> Journals { get; set; }

        public GetAllJournalsView()
        {
            Journals = new List<GetAllJournalsViewItem>();
        }
    }

    public class GetAllJournalsViewItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Cost { get; set; }
    }
}
