﻿namespace BookStore.BusinessLogic.Views
{
    public class CreateJournalView
    {
        public string Name { get; set; }
        public int Cost { get; set; }
    }
}
