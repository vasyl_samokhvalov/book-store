﻿namespace BookStore.BusinessLogic.Views
{
    public class UpdateJournalView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Cost { get; set; }
    }
}
