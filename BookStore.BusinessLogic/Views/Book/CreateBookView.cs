﻿namespace BookStore.BusinessLogic.Views
{
    public class CreateBookView
    {
        public string Name { get; set; }
        public int Cost { get; set; }
        public int Publisher { get; set; }
        public int[] Author { get; set; }
        public int Year { get; set; }
    }
}
