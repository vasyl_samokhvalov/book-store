﻿namespace BookStore.BusinessLogic.Views
{
    public class UpdateBookView
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Cost { get; set; }
        public int Publisher { get; set; }
        public int Year { get; set; }
    }
}
