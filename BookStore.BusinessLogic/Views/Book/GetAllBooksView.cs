﻿using System.Collections.Generic;

namespace BookStore.BusinessLogic.Views
{
    public class GetAllBooksView
    {
        public List<GetAllBooksViewItem> Books { get; set; }

        public GetAllBooksView()
        {
            Books = new List<GetAllBooksViewItem>();
        }
    }

    public class GetAllBooksViewItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Cost { get; set; }
        public int Publisher { get; set; }
        public int Year { get; set; }
        public List<int> Authors { get; set; }

        public GetAllBooksViewItem()
        {
            Authors = new List<int>();
        }
    }
}

