﻿namespace BookStore.BusinessLogic.Views
{
    public class CreateUserView
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
