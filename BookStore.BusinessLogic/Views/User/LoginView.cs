﻿namespace BookStore.BusinessLogic.Views
{
    public class LoginUserView
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
