﻿using System.Collections.Generic;

namespace BookStore.BusinessLogic.Views
{
    public class GetAllUsersView
    {
        public List<GetAllUsersViewItem> Users { get; set; }

        public GetAllUsersView()
        {
            Users = new List<GetAllUsersViewItem>();
        }
    }

    public class GetAllUsersViewItem
    {
        public int Id;
        public string Name;
    }
}
