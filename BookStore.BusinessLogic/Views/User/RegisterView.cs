﻿namespace BookStore.BusinessLogic.Views
{
    public class RegisterView
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
    }
}
