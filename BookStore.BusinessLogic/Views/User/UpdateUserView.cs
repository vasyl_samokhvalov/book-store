﻿namespace BookStore.BusinessLogic.Views
{
    public class UpdateUserView
    {
        public string Id { get; set; }
        public string Email { get; set; }
    }
}
