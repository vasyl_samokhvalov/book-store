﻿namespace BookStore.BusinessLogic.Views
{
    public class ChangePasswordView
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string NewPassword { get; set; }
    }
}
