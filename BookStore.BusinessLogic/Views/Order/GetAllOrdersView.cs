﻿using System.Collections.Generic;

namespace BookStore.BusinessLogic.Views
{
    public class GetAllOrdersView
    {
        public List<GetAllOrdersViewItem> Orders { get; set; }

        public GetAllOrdersView()
        {
            Orders = new List<GetAllOrdersViewItem>();
        }
    }

    public class GetAllOrdersViewItem
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int Cost { get; set; }
    }
}
