﻿using System.Collections.Generic;

namespace BookStore.BusinessLogic.Views
{
    public class GetUserOrderView
    {
        public List<GetUserOrderViewItem> Orders { get; set; }

        public GetUserOrderView()
        {
            Orders = new List<GetUserOrderViewItem>();
        }
    }

    public class GetUserOrderViewItem
    {
        public int Id { get; set; }
        public int Cost { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
    }
}
