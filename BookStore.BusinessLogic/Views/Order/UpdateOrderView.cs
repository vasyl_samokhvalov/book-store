﻿namespace BookStore.BusinessLogic.Views
{
    public class UpdateOrderView
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int Cost { get; set; }
    }
}
