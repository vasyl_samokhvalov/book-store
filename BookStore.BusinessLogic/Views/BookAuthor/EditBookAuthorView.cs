﻿using BookStore.DataAccess.Entities;

namespace BookStore.BusinessLogic.Views
{
    public class EditBookAuthorView
    {
        public int BookId { get; set; }
        public Book Book { get; set; }
        public int AuthorId { get; set; }
        public Author Author { get; set; }
    }
}
