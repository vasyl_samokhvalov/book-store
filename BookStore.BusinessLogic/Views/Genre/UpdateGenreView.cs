﻿namespace BookStore.BusinessLogic.Views
{
    public class UpdateGenreView
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
