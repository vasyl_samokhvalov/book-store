﻿using System.Collections.Generic;

namespace BookStore.BusinessLogic.Views
{
    public class GetAllGenresView
    {
        public List<GetAllGenresViewItem> Genres { get; set; }

        public GetAllGenresView()
        {
            Genres = new List<GetAllGenresViewItem>();
        }
    }

    public class GetAllGenresViewItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
