import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UpdateOrderViewItem, GetAllOrdersViewItem } from 'src/shared/models/order/order.view';
import { isNullOrUndefined } from 'util';
import { OrderService } from 'src/shared/services/order.service';

export interface DialogData {
  orders: GetAllOrdersViewItem[]
}

@Component({
  selector: 'update-order-popup',
  templateUrl: 'update-order-popup.component.html',
  styleUrls: ['./update-order-popup.component.css']
})

export class UpdateOrderPopup {
  public updateOrderView: UpdateOrderViewItem = new UpdateOrderViewItem()
  public publisher
  public author
  public updateOrderPublisher: string

  constructor(
    public dialogRef: MatDialogRef<UpdateOrderPopup>,
    public orderService: OrderService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close()
  }

  public close() {
    this.dialogRef.close()
  }

  public getOrder() {
    if (!isNullOrUndefined(this.updateOrderView.id)) {
      this.updateOrderView.userId = this.data.orders[this.updateOrderView.id].userId
      this.updateOrderView.cost = this.data.orders[this.updateOrderView.id].cost
    }
  }

  public updateOrder() {
    if(isNullOrUndefined(this.updateOrderView.id)) {
      this.close()
      return
    }
    this.orderService.updateOrder(this.updateOrderView).subscribe()
    this.close()
  }
}
