import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AddOrderViewItem } from 'src/shared/models/order/order.view';
import { OrderService } from 'src/shared/services/order.service';

export interface DialogData {
}

@Component({
    selector: 'add-order-popup',
    templateUrl: 'add-order-popup.component.html',
    styleUrls: ['./add-order-popup.component.css']
})

export class AddOrderPopup {
    public addOrderView: AddOrderViewItem = new AddOrderViewItem()

    constructor(
        public dialogRef: MatDialogRef<AddOrderPopup>,
        private orderService: OrderService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close()
    }

    public close() {
        this.dialogRef.close()
    }

    public addOrder() {
        this.orderService.addOrder(this.addOrderView).subscribe()
        this.close()
    }
}
