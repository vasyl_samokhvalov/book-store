import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../../shared/services/order.service';
import { GetAllOrdersView, GetAllOrdersViewItem, AddOrderViewItem, UpdateOrderViewItem, GetAllOrdersBigView } from '../../../shared/models/order/order.view';
import 'core-js/es7/reflect';
import { CookieService } from 'ngx-cookie-service';
import {Router} from "@angular/router"
import { isNullOrUndefined } from 'util';
import { MatDialog } from '@angular/material';
import { AddOrderPopup } from './popups/add-order-popup/add-order-popup.component';
import { UpdateOrderPopup } from './popups/update-order-popup/update-order-popup.component';
import { DeleteOrderPopup } from './popups/delete-order-popup/delete-order-popup.component';

export interface DialogData {
  orders: GetAllOrdersViewItem[]
}

@Component({
  selector: 'orders-app',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})

export class OrderComponent implements OnInit {
  public orders: GetAllOrdersView = new GetAllOrdersView()
  public dataBig: GetAllOrdersBigView = new GetAllOrdersBigView()
  public updateOrderView: UpdateOrderViewItem = new UpdateOrderViewItem()
  public addOrderView: AddOrderViewItem = new AddOrderViewItem()
  public isAscending: boolean = true
  public searchText: string

  constructor(private orderService: OrderService, private cookie: CookieService, private router: Router, private dialog: MatDialog) { }


  ngOnInit() {
    this.checkRole();
    this.loadOrders();
  }

  public checkRole() {
    if (this.cookie.get("UserRole") != "admin") {
      this.router.navigate(['orders'])
    }
  }

  private loadOrders(){
    this.orderService.getAll().subscribe(data => { this.orders = data ;});
  }

  openAddPopup(): void {
    const dialogRef = this.dialog.open(AddOrderPopup, {
      width: '330px',
      data: {}
    })

    dialogRef.afterClosed().subscribe(result => {
      this.loadOrders()
    })
  }

  openUpdatePopup(): void {
    const dialogRef = this.dialog.open(UpdateOrderPopup, {
      width: '330px',
      data: { orders: this.orders.orders }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.loadOrders()
    })
  }

  openDeletePopup(): void {
    const dialogRef = this.dialog.open(DeleteOrderPopup, {
      width: '330px',
      data: {}
    })

    dialogRef.afterClosed().subscribe(result => {
      this.loadOrders()
    })
  }

  public sortByDbIdAsc() {
    this.orders.orders.sort((a, b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0))
    this.searchText = ''
  }

  private sortByUserId(){
    if(this.isAscending){
      this.sortByUserIdDesc()
      this.isAscending = false
      return
    }
    this.sortByUserIdAsc()
    this.isAscending = true
  }

  public sortByUserIdDesc() {
    this.orders.orders.sort((a, b) => (a.userId > b.userId) ? 1 : ((b.userId > a.userId) ? -1 : 0))
  }

  public sortByUserIdAsc() {
    this.orders.orders.sort((a, b) => (a.userId < b.userId) ? 1 : ((b.userId < a.userId) ? -1 : 0))
  }

  public deleteOrder(id: number){
    this.orderService.deleteOrder(id).subscribe(data =>{this.loadOrders()})
  }

  public getUserOrder(userId: string){
    this.orderService.getUserOrder(userId).subscribe(data =>{console.log(data)})
  }
}