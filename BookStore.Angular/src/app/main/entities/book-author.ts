import { Book } from './book';
import { Author } from './author';

export class BookAuthor{
    public id: number

    public book: Book
    
    public author: Author
}