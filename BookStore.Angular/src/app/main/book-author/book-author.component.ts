import { Component, OnInit } from '@angular/core';
import { BookAuthorService } from '../../../shared/services/book-author.service';
import { GetAllBookAuthorsView, GetAllBookAuthorsViewItem, AddBookAuthorViewItem, DeleteBookAuthorViewItem, UpdateBookAuthorViewItem } from '../../../shared/models/book-author/book-author.view';
import 'core-js/es7/reflect';

@Component({
  selector: 'bookauthors-app',
  templateUrl: './bookauthor.component.html'
})

export class BookAuthorComponent implements OnInit {
  public data: GetAllBookAuthorsView = new GetAllBookAuthorsView();

  constructor(private bookauthorService: BookAuthorService) { }


  ngOnInit() {
    this.loadBookAuthors();
  }

  private loadBookAuthors(){
    this.bookauthorService.getAll().subscribe(data => { this.data = data ;});
  }


  public addBookAuthor(bookId: number, authorId: number){
    var temp: AddBookAuthorViewItem = new AddBookAuthorViewItem()
    this.bookauthorService.addBookAuthor(temp).subscribe()
  }

  public deleteBookAuthor(id: number){
    console.log(id)
    this.bookauthorService.deleteBookAuthor(id).subscribe()
  }
  
  private updateBookAuthor(id: number, name: string, cost:number, year:number, publisher: number){
    var temp: UpdateBookAuthorViewItem =  new UpdateBookAuthorViewItem()
    temp.id = id
    this.bookauthorService.updateBookAuthor(temp).subscribe()
  }
}