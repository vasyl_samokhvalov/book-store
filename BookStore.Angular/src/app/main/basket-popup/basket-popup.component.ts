import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { isNullOrUndefined } from 'util';

export interface DialogData {
    rawBookBasket: string
    rawJournalBasket: string
    bookBasket: string[]
    journalBasket: string[]
    books: string[]
    journals: string[]
}

@Component({
    selector: 'basket-popup',
    templateUrl: 'basket-popup.component.html',
    styleUrls: ['./basket-popup.component.css']
})
export class BasketPopup {
    public isAny: boolean = false
    public isBooks: boolean = false
    public isJournals: boolean = false

    constructor(
        public dialogRef: MatDialogRef<BasketPopup>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

    ngOnInit() {
        this.data.rawBookBasket = localStorage.getItem('BookBasket')
        this.data.rawJournalBasket = localStorage.getItem('JournalBasket')
        if (!isNullOrUndefined(this.data.rawBookBasket) && this.data.rawBookBasket.length > 2) {
            this.isBooks = true
        }
        if (!isNullOrUndefined(this.data.rawJournalBasket) && this.data.rawJournalBasket.length > 2) {
            this.isJournals = true
        }
        if (this.isBooks || this.isJournals) {
            this.isAny = true
        }
        // parsing books
        this.data.books = []
        this.data.bookBasket = JSON.parse(this.data.rawBookBasket)
        for (let book of this.data.bookBasket) {
            this.data.books.push(JSON.parse(book)['name'])
        }
        // parsing journals
        this.data.journals = []
        this.data.journalBasket = JSON.parse(this.data.rawJournalBasket)
        for (let journal of this.data.journalBasket) {
            this.data.journals.push(JSON.parse(journal)['name'])
        }
        
    }

    onNoClick(): void {
        this.dialogRef.close()
    }

    public removeBookFromBasket(bookName: string) {
        var temp = this.data.books.indexOf(bookName)
        if (!isNullOrUndefined(temp) && (temp > -1)) {
            this.data.bookBasket.splice(temp, 1)
            localStorage.setItem("BookBasket", JSON.stringify(this.data.bookBasket))
        }
        this.data.rawBookBasket = localStorage.getItem('BookBasket')
        this.data.books = []
        this.data.bookBasket = JSON.parse(this.data.rawBookBasket)
        for (let book of this.data.bookBasket) {
            this.data.books.push(JSON.parse(book)['name'])
        }
    }

    public removeJournalFromBasket(journalName: string) {
        var temp = this.data.journals.indexOf(journalName)
        if (!isNullOrUndefined(temp) && (temp > -1)) {
            this.data.journalBasket.splice(temp, 1)
            localStorage.setItem("JournalBasket", JSON.stringify(this.data.journalBasket))
        }
        this.data.rawJournalBasket = localStorage.getItem('JournalBasket')
        this.data.journals = []
        this.data.journalBasket = JSON.parse(this.data.rawJournalBasket)
        for (let journal of this.data.journalBasket) {
            this.data.journals.push(JSON.parse(journal)['name'])
        }
    }
}