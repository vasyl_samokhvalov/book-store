import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AddJournalViewItem, GetAllJournalsViewItem } from 'src/shared/models/journal/journal.view';
import { JournalService } from 'src/shared/services/journal.service';

export interface DialogData {
    journals: GetAllJournalsViewItem[]
}

@Component({
    selector: 'add-journal-popup',
    templateUrl: 'add-journal-popup.component.html',
    styleUrls: ['./add-journal-popup.component.css']
})

export class AddJournalPopup {
    public addJournalView: AddJournalViewItem = new AddJournalViewItem()

    constructor(
        public dialogRef: MatDialogRef<AddJournalPopup>,
        private journalService: JournalService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close()
    }

    public close() {
        this.dialogRef.close()
    }

    public addJournal() {
        this.journalService.addJournal(this.addJournalView).subscribe()
        this.close()
    }
}
