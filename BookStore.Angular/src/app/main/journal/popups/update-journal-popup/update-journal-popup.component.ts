import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UpdateJournalViewItem, GetAllJournalsViewItem } from 'src/shared/models/journal/journal.view';
import { isNullOrUndefined } from 'util';
import { JournalService } from 'src/shared/services/journal.service';

export interface DialogData {
  journals: GetAllJournalsViewItem[]
}

@Component({
  selector: 'update-journal-popup',
  templateUrl: 'update-journal-popup.component.html',
  styleUrls: ['./update-journal-popup.component.css']
})

export class UpdateJournalPopup {
  public updateJournalView: UpdateJournalViewItem = new UpdateJournalViewItem()
  public publisher
  public author
  public updateJournalPublisher: string

  constructor(
    public dialogRef: MatDialogRef<UpdateJournalPopup>,
    public journalService: JournalService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close()
  }

  public close() {
    this.dialogRef.close()
  }

  public getJournal() {
    if (!isNullOrUndefined(this.updateJournalView.id)) {
      this.updateJournalView.name = this.data.journals[this.updateJournalView.id].name
      this.updateJournalView.cost = this.data.journals[this.updateJournalView.id].cost
    }
  }

  public updateJournal() {
    if(isNullOrUndefined(this.updateJournalView.id)) {
      this.close()
      return
    }
    var temp: UpdateJournalViewItem = new UpdateJournalViewItem()
    temp = this.data.journals[this.updateJournalView.id]
    if (!isNullOrUndefined(this.updateJournalView.name)) {
      temp.name = this.updateJournalView.name
    }
    if (!isNullOrUndefined(this.updateJournalView.cost)) {
      temp.cost = this.updateJournalView.cost
    }
    this.journalService.updateJournal(temp).subscribe()
    this.close()
  }
}
