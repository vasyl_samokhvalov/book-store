import { OnInit, Component } from '@angular/core';
import { JournalService } from '../../../shared/services/journal.service';
import 'core-js/es7/reflect';
import { isNullOrUndefined } from 'util';
import { CookieService } from 'ngx-cookie-service';
import { MatDialog } from '@angular/material';
import { GetAllJournalsViewItem, GetAllJournalsView, UpdateJournalViewItem, AddJournalViewItem } from 'src/shared/models/journal/journal.view';
import { AddJournalPopup } from './popups/add-journal-popup/add-journal-popup.component';
import { DeleteJournalPopup } from './popups/delete-journal-popup/delete-journal-popup.component';
import { UpdateJournalPopup } from './popups/update-journal-popup/update-journal-popup.component';

export interface DialogData {
  journals: GetAllJournalsViewItem
}

@Component({
  selector: 'journals-app',
  templateUrl: './journal.component.html',
  styleUrls: ['./journal.component.css']
})

export class JournalComponent implements OnInit {
  public journals: GetAllJournalsView = new GetAllJournalsView()
  public updateJournalView: UpdateJournalViewItem = new UpdateJournalViewItem()
  public addJournalView: AddJournalViewItem = new AddJournalViewItem()
  public isVisible: boolean = false;
  public isAscending: boolean = true
  public basket: string[] = []
  public searchText: string

  constructor(private journalService: JournalService, private cookie: CookieService, private dialog: MatDialog) { }

  ngOnInit() {
    this.loadJournals();
    this.checkRole();
    if (!isNullOrUndefined(localStorage.getItem("JournalBasket"))) { this.basket = JSON.parse(localStorage.getItem("JournalBasket")) }
  }

  private loadJournals() {
    this.journalService.getAll().subscribe(data => { this.journals = data; });
  }

  openAddPopup(): void {
    const dialogRef = this.dialog.open(AddJournalPopup, {
      width: '330px',
      data: { }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.loadJournals()
    })
  }

  openUpdatePopup(): void {
    const dialogRef = this.dialog.open(UpdateJournalPopup, {
      width: '330px',
      data: { journals: this.journals.journals }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.loadJournals()
    })
  }

  openDeletePopup(): void {
    const dialogRef = this.dialog.open(DeleteJournalPopup, {
      width: '330px',
      data: { }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.loadJournals()
    })
  }


  public sortByDbIdAsc() {
    this.journals.journals.sort((a, b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0))
    this.searchText = ''
  }

  private sortByName() {
    if (this.isAscending) {
      this.sortByNameDesc()
      this.isAscending = false
      return
    }
    this.sortByNameAsc()
    this.isAscending = true
  }

  public sortByNameDesc() {
    this.journals.journals.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
  }

  public sortByNameAsc() {
    this.journals.journals.sort((a, b) => (a.name < b.name) ? 1 : ((b.name < a.name) ? -1 : 0))
  }

  private sortByCost() {
    if (this.isAscending) {
      this.sortByCostDesc()
      this.isAscending = false
      return
    }
    this.sortByCostAsc()
    this.isAscending = true
  }

  private sortByCostDesc() {
    this.journals.journals.sort((a, b) => (a.cost > b.cost) ? 1 : ((b.cost > a.cost) ? -1 : 0))
  }

  private sortByCostAsc() {
    this.journals.journals.sort((a, b) => (a.cost < b.cost) ? 1 : ((b.cost < a.cost) ? -1 : 0))
  }

  private updateJournal() {
    var temp: UpdateJournalViewItem = new UpdateJournalViewItem()
    temp = this.journals.journals[this.updateJournalView.id]
    if (!isNullOrUndefined(this.updateJournalView.name)) {
      temp.name = this.updateJournalView.name
    }
    if (!isNullOrUndefined(this.updateJournalView.cost)) {
      temp.cost = this.updateJournalView.cost
    }
    this.journalService.updateJournal(temp).subscribe(data => { this.loadJournals() })
  }

  public checkRole() {
    if (this.cookie.get("UserRole") == "admin") {
      this.isVisible = true;
    }
  }

  public getJournal() {
    if (!isNullOrUndefined(this.updateJournalView.id)) {
      this.updateJournalView.name = this.journals.journals[this.updateJournalView.id].name
      this.updateJournalView.cost = this.journals.journals[this.updateJournalView.id].cost
    }
  }

  public addToBasket(journalId: number) {
    if (!isNullOrUndefined(localStorage.getItem("JournalBasket"))) { this.basket = JSON.parse(localStorage.getItem("JournalBasket")) }
    this.basket[this.basket.length] = JSON.stringify(this.journals.journals[journalId - 1])
    this.basket.sort((a, b) => (a > b) ? 1 : ((b > a) ? -1 : 0))
    localStorage.setItem("JournalBasket", JSON.stringify(this.basket))
  }

  public removeFromBasket(journalId: number) {
    if (!isNullOrUndefined(localStorage.getItem("JournalBasket"))) { this.basket = JSON.parse(localStorage.getItem("JournalBasket")) }
    var temp = JSON.stringify(this.journals.journals[journalId - 1])
    if (!isNullOrUndefined(this.basket.indexOf(temp)) && (this.basket.indexOf(temp) > -1)) {
      this.basket.splice(this.basket.indexOf(temp), 1)
      localStorage.setItem("JournalBasket", JSON.stringify(this.basket))
    }
  }
}