import { Component, OnInit } from '@angular/core';
import { BookService } from '../../../shared/services/book.service';
import { GetAllBooksView, AddBookViewItem, UpdateBookViewItem } from '../../../shared/models/book/book.view';
import 'core-js/es7/reflect';
import { PublisherService } from '../../../shared/services/publisher.service';
import { GetAllPublishersView } from '../../../shared/models/publisher/publisher.view';
import { isNullOrUndefined } from 'util';
import { Token } from '../../../shared/models/token/token.view';
import { LoginComponent } from '../../login/login.component';
import { CookieService } from 'ngx-cookie-service';
import { GetAllGenresView } from 'src/shared/models/genre/genre.view';
import { GetAllAuthorsView } from 'src/shared/models/author/GetAllAuthorsView';
import { AuthorService } from 'src/shared/services/author.service';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { AddBookPopup } from './popups/add-book-popup/add-book-popup.component';
import { UpdateBookPopup } from './popups/update-book-popup/update-book-popup.component';
import { DeleteBookPopup } from './popups/delete-book-popup/delete-book-popup.component';

export interface DialogData {
  publishers: GetAllPublishersView[],
  authors: GetAllAuthorsView[]
}

@Component({
  selector: 'books-app',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})

export class BookComponent implements OnInit {
  public books: GetAllBooksView = new GetAllBooksView()
  public publishers: GetAllPublishersView = new GetAllPublishersView()
  public genres: GetAllGenresView = new GetAllGenresView()
  public authors: GetAllAuthorsView = new GetAllAuthorsView()
  public isVisible: boolean = false
  public isAscending: boolean = true
  public token: Token = new Token()
  public updateBookView: UpdateBookViewItem = new UpdateBookViewItem()
  public addBookView: AddBookViewItem = new AddBookViewItem()
  public basket: string[] = []
  public searchText: string
  public updateBookPublisher: string
  public publisherParam: number
  public authorParam: number
  public fromCost: number
  public toCost: number
  public author
  public publisher

  constructor(private activatedRoute: ActivatedRoute, private bookService: BookService, private publisherService: PublisherService, private login: LoginComponent, private cookie: CookieService, public authorService: AuthorService, public dialog: MatDialog) { }

  ngOnInit() {
    this.checkRole()
    this.publisherParam = this.activatedRoute.snapshot.queryParams['publisher'];
    this.authorParam = this.activatedRoute.snapshot.queryParams['author'];
    // this.activatedRoute.queryParams.subscribe(params => {this.searchText = params.keyword || null;})
    this.loadBooks()
    if (!isNullOrUndefined(localStorage.getItem("BookBasket"))) { this.basket = JSON.parse(localStorage.getItem("BookBasket")) }
  }

  private loadBooks() {
    this.bookService.getAll().subscribe(data => {
      this.books = data;
      this.loadAuthors()
      this.loadPublishers()
    });
  }

  private loadPublishers() {
    this.publisherService.getAll().subscribe(publishers => {
      this.publishers = publishers
      for (let book of this.books.books) {
        book.publisherName = this.publishers.publishers[book.publisher - 1].name
      }
      if (!isNullOrUndefined(this.publisherParam)) { this.searchText = publishers.publishers[this.publisherParam - 1].name }
    })
  }

  private loadAuthors() {
    this.authorService.getAll().subscribe(authors => {
      this.authors = authors
      for (let book of this.books.books) {
        book.authorsNames = ''
        for (let author of book.authors) {
          if (book.authorsNames != '') { book.authorsNames += ', ' }
          book.authorsNames += this.authors.authors[author - 1].name + ' ' + this.authors.authors[author - 1].surname
        }
      }
      if (!isNullOrUndefined(this.authorParam)) { this.searchText = authors.authors[this.authorParam - 1].name + ' ' + authors.authors[this.authorParam - 1].surname }
    })
  }

  openAddPopup(): void {
    const dialogRef = this.dialog.open(AddBookPopup, {
      width: '330px',
      data: { publishers: this.publishers.publishers, authors: this.authors.authors }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.loadBooks()
    })
  }

  openUpdatePopup(): void {
    const dialogRef = this.dialog.open(UpdateBookPopup, {
      width: '330px',
      data: { books: this.books.books, publishers: this.publishers.publishers, authors: this.authors.authors }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.loadBooks()
    })
  }

  openDeletePopup(): void {
    const dialogRef = this.dialog.open(DeleteBookPopup, {
      width: '330px',
      data: {}
    })

    dialogRef.afterClosed().subscribe(result => {
      this.loadBooks()
    })
  }

  public sortByDbIdAsc() {
    this.books.books.sort((a, b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0))
    this.searchText = ''
  }

  private sortByName() {
    if (this.isAscending) {
      this.sortByNameDesc()
      this.isAscending = false
      return
    }
    this.sortByNameAsc()
    this.isAscending = true
  }

  public sortByNameDesc() {
    this.books.books.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
  }

  public sortByNameAsc() {
    this.books.books.sort((a, b) => (a.name < b.name) ? 1 : ((b.name < a.name) ? -1 : 0))
  }

  private sortByCost() {
    if (this.isAscending) {
      this.sortByCostDesc()
      this.isAscending = false
      return
    }
    this.sortByCostAsc()
    this.isAscending = true
  }

  private sortByCostDesc() {
    this.books.books.sort((a, b) => (a.cost > b.cost) ? 1 : ((b.cost > a.cost) ? -1 : 0))
  }

  private sortByCostAsc() {
    this.books.books.sort((a, b) => (a.cost < b.cost) ? 1 : ((b.cost < a.cost) ? -1 : 0))
  }

  private sortByYear() {
    if (this.isAscending) {
      this.sortByYearDesc()
      this.isAscending = false
      return
    }
    this.sortByYearAsc()
    this.isAscending = true
  }

  private sortByYearDesc() {
    this.books.books.sort((a, b) => (a.year > b.year) ? 1 : ((b.year > a.year) ? -1 : 0))
  }

  private sortByYearAsc() {
    this.books.books.sort((a, b) => (a.year < b.year) ? 1 : ((b.year < a.year) ? -1 : 0))
  }

  private sortByPublisher() {
    if (this.isAscending) {
      this.sortByPublisherDesc()
      this.isAscending = false
      return
    }
    this.sortByPublisherAsc()
    this.isAscending = true
  }

  private sortByPublisherDesc() {
    this.books.books.sort((a, b) => (a.publisher > b.publisher) ? 1 : ((b.publisher > a.publisher) ? -1 : 0))
  }

  private sortByPublisherAsc() {
    this.books.books.sort((a, b) => (a.publisher < b.publisher) ? 1 : ((b.publisher < a.publisher) ? -1 : 0))
  }

  public checkRole() {
    if (this.cookie.get("UserRole") == "admin") {
      this.isVisible = true;
    }
  }

  public addToBasket(bookId: number) {
    if (!isNullOrUndefined(localStorage.getItem("BookBasket"))) { this.basket = JSON.parse(localStorage.getItem("BookBasket")) }
    this.basket[this.basket.length] = JSON.stringify(this.books.books[bookId - 1])
    this.basket.sort((a, b) => (a > b) ? 1 : ((b > a) ? -1 : 0))
    localStorage.setItem("BookBasket", JSON.stringify(this.basket))
  }

  public removeFromBasket(bookId: number) {
    if (!isNullOrUndefined(localStorage.getItem("BookBasket"))) { this.basket = JSON.parse(localStorage.getItem("BookBasket")) }
    var temp = JSON.stringify(this.books.books[bookId - 1])
    if (!isNullOrUndefined(this.basket.indexOf(temp)) && (this.basket.indexOf(temp) > -1)) {
      this.basket.splice(this.basket.indexOf(temp), 1)
      localStorage.setItem("BookBasket", JSON.stringify(this.basket))
    }
  }
}

