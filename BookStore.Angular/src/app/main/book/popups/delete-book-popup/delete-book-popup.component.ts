import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GetAllPublishersViewItem } from 'src/shared/models/publisher/publisher.view';
import { GetAllAuthorsViewItem } from 'src/shared/models/author/GetAllAuthorsView';
import { GetAllBooksViewItem } from 'src/shared/models/book/book.view';
import { BookService } from 'src/shared/services/book.service';

export interface DialogData {
    books: GetAllBooksViewItem[]
    publishers: GetAllPublishersViewItem[]
    authors: GetAllAuthorsViewItem[]
}

@Component({
    selector: 'delete-book-popup',
    templateUrl: 'delete-book-popup.component.html',
    styleUrls: ['./delete-book-popup.component.css']
})

export class DeleteBookPopup {
    public deleteId: number
    public publisher
    public author

    constructor(
        public dialogRef: MatDialogRef<DeleteBookPopup>,
        public bookService: BookService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close()
    }

    public close(){
        this.dialogRef.close()
    }
    
    public deleteBook() {
        this.bookService.deleteBook(this.deleteId).subscribe()
        this.close()
    }
}
