import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GetAllPublishersViewItem } from 'src/shared/models/publisher/publisher.view';
import { GetAllAuthorsViewItem } from 'src/shared/models/author/GetAllAuthorsView';
import { UpdateBookViewItem, GetAllBooksViewItem } from 'src/shared/models/book/book.view';
import { isNullOrUndefined } from 'util';
import { BookService } from 'src/shared/services/book.service';

export interface DialogData {
  books: GetAllBooksViewItem[]
  publishers: GetAllPublishersViewItem[]
  authors: GetAllAuthorsViewItem[]
}

@Component({
  selector: 'update-book-popup',
  templateUrl: 'update-book-popup.component.html',
  styleUrls: ['./update-book-popup.component.css']
})

export class UpdateBookPopup {
  public updateBookView: UpdateBookViewItem = new UpdateBookViewItem()
  public publisher
  public author
  public updateBookPublisher: string

  constructor(
    public dialogRef: MatDialogRef<UpdateBookPopup>,
    public bookService: BookService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close()
  }

  public close() {
    this.dialogRef.close()
  }

  public getBook() {
    if (!isNullOrUndefined(this.updateBookView.id)) {
      this.updateBookView.name = this.data.books[this.updateBookView.id].name
      this.updateBookView.cost = this.data.books[this.updateBookView.id].cost
      this.updateBookView.year = this.data.books[this.updateBookView.id].year
      this.updateBookView.publisher = this.data.books[this.updateBookView.id].publisher
      this.updateBookPublisher = this.data.publishers[this.updateBookView.publisher - 1].name
    }
  }

  public updateBook() {
    if(isNullOrUndefined(this.updateBookView.id)) {
      this.close()
      return
    }
    var temp: UpdateBookViewItem = new UpdateBookViewItem()
    temp = this.data.books[this.updateBookView.id]
    if (!isNullOrUndefined(this.updateBookView.name)) {
      temp.name = this.updateBookView.name
    }
    if (!isNullOrUndefined(this.updateBookView.cost)) {
      temp.cost = this.updateBookView.cost
    }
    if (!isNullOrUndefined(this.updateBookView.year)) {
      temp.year = this.updateBookView.year
    }
    if (!isNullOrUndefined(this.updateBookView.publisher)) {
      temp.publisher = this.updateBookView.publisher
    }
    this.bookService.updateBook(temp).subscribe()
    this.close()
  }
}
