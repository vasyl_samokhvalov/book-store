import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GetAllPublishersViewItem } from 'src/shared/models/publisher/publisher.view';
import { GetAllAuthorsViewItem } from 'src/shared/models/author/GetAllAuthorsView';
import { AddBookViewItem } from 'src/shared/models/book/book.view';
import { BookService } from 'src/shared/services/book.service';

export interface DialogData {
    publishers: GetAllPublishersViewItem[],
    authors: GetAllAuthorsViewItem[]
}

@Component({
    selector: 'add-book-popup',
    templateUrl: 'add-book-popup.component.html',
    styleUrls: ['./add-book-popup.component.css']
})

export class AddBookPopup {
    public addBookView: AddBookViewItem = new AddBookViewItem()
    public publisher
    public author

    constructor(
        public dialogRef: MatDialogRef<AddBookPopup>,
        private bookService: BookService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close()
    }

    public close() {
        this.dialogRef.close()
    }

    public addBook() {
        this.addBookView.author = this.author
        this.addBookView.publisher = this.publisher
        this.bookService.addBook(this.addBookView).subscribe()
        this.close()
    }
}
