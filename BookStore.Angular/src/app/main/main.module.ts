import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { NotFoundComponent } from '../not-found/not-found.component'
import { BookComponent } from '../main/book/book.component'
import { GenreComponent } from '../main/genre/genre.component'
import { JournalComponent } from '../main/journal/journal.component'
import { OrderComponent } from '../main/order/order.component'
import { PublisherComponent } from '../main/publisher/publisher.component'
import { UserComponent } from '../main/user/user.component'
import { LoginComponent } from '../login/login.component'
import { RegisterComponent } from '../register/register.component'
import { CookieService } from 'ngx-cookie-service'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NavigationBarComponent } from '../main/navigation-bar/navigation-bar.component'
import { LayoutModule } from '@angular/cdk/layout'
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatDialogModule, MatFormFieldModule, MatSelectModule, MatTooltipModule } from '@angular/material'
import { AuthorComponent } from '../main/author/author.component'
import { LoginGuard } from '../login-guard.component'
import { CabinetComponent } from '../main/cabinet/cabinet.component'
import { ErrorInterceptor } from 'src/shared/modules/error-handler.module'
import { Ng2SearchPipeModule } from 'ng2-search-filter'
import { BasketPopup } from '../main/basket-popup/basket-popup.component'
import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { AddBookPopup } from './book/popups/add-book-popup/add-book-popup.component';
import { UpdateBookPopup } from './book/popups/update-book-popup/update-book-popup.component';
import { DeleteBookPopup } from './book/popups/delete-book-popup/delete-book-popup.component';
import { AddAuthorPopup } from './author/popups/add-author-popup/add-author-popup.component';
import { UpdateAuthorPopup } from './author/popups/update-author-popup/update-author-popup.component';
import { DeleteAuthorPopup } from './author/popups/delete-author-popup/delete-author-popup.component';
import { AddGenrePopup } from './genre/popups/add-genre-popup/add-genre-popup.component';
import { DeleteGenrePopup } from './genre/popups/delete-genre-popup/delete-genre-popup.component';
import { UpdateGenrePopup } from './genre/popups/update-genre-popup/update-genre-popup.component';
import { AddJournalPopup } from './journal/popups/add-journal-popup/add-journal-popup.component';
import { UpdateJournalPopup } from './journal/popups/update-journal-popup/update-journal-popup.component';
import { DeleteJournalPopup } from './journal/popups/delete-journal-popup/delete-journal-popup.component';
import { DeletePublisherPopup } from './publisher/popups/delete-publisher-popup/delete-publisher-popup.component';
import { UpdatePublisherPopup } from './publisher/popups/update-publisher-popup/update-publisher-popup.component';
import { AddPublisherPopup } from './publisher/popups/add-publisher-popup/add-publisher-popup.component';
import { AddOrderPopup } from './order/popups/add-order-popup/add-order-popup.component';
import { UpdateOrderPopup } from './order/popups/update-order-popup/update-order-popup.component';
import { DeleteOrderPopup } from './order/popups/delete-order-popup/delete-order-popup.component';


@NgModule({
    imports: [BrowserModule,
        FormsModule,
        HttpClientModule,
        MainRoutingModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        LayoutModule,
        MatToolbarModule,
        MatFormFieldModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule,
        MatDialogModule,
        MatSelectModule,
        Ng2SearchPipeModule,
        BrowserAnimationsModule,
        MatTooltipModule],
    declarations: [NotFoundComponent,
        BookComponent,
        AuthorComponent,
        GenreComponent,
        JournalComponent,
        OrderComponent,
        PublisherComponent,
        UserComponent,
        RegisterComponent,
        CabinetComponent,
        BasketPopup,
        AddBookPopup,
        UpdateBookPopup,
        DeleteBookPopup,
        AddAuthorPopup,
        UpdateAuthorPopup,
        DeleteAuthorPopup,
        AddGenrePopup,
        UpdateGenrePopup,
        DeleteGenrePopup,
        AddJournalPopup,
        UpdateJournalPopup,
        DeleteJournalPopup,
        AddPublisherPopup,
        UpdatePublisherPopup,
        DeletePublisherPopup,
        AddOrderPopup,
        UpdateOrderPopup,
        DeleteOrderPopup],
    bootstrap: [MainComponent,
        NavigationBarComponent],
    entryComponents: [BasketPopup,
        AddBookPopup,
        UpdateBookPopup,
        DeleteBookPopup,
        AddAuthorPopup,
        UpdateAuthorPopup,
        DeleteAuthorPopup,
        AddGenrePopup,
        UpdateGenrePopup,
        DeleteGenrePopup,
        AddJournalPopup,
        UpdateJournalPopup,
        DeleteJournalPopup,
        AddPublisherPopup,
        UpdatePublisherPopup,
        DeletePublisherPopup,
        AddOrderPopup,
        UpdateOrderPopup,
        DeleteOrderPopup,
        NavigationBarComponent],
    providers: [LoginComponent,
        CookieService,
        LoginGuard,
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }]
})
export class MainModule {
}