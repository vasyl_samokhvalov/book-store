import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UpdateGenreViewItem, GetAllGenresViewItem } from 'src/shared/models/genre/genre.view';
import { isNullOrUndefined } from 'util';
import { GenreService } from 'src/shared/services/genre.service';

export interface DialogData {
  genres: GetAllGenresViewItem[]
}

@Component({
  selector: 'update-genre-popup',
  templateUrl: 'update-genre-popup.component.html',
  styleUrls: ['./update-genre-popup.component.css']
})

export class UpdateGenrePopup {
  public updateGenreView: UpdateGenreViewItem = new UpdateGenreViewItem()

  constructor(
    public dialogRef: MatDialogRef<UpdateGenrePopup>,
    public genreService: GenreService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit() {
    console.log(this.data.genres)
  }

  onNoClick(): void {
    this.dialogRef.close()
  }

  public close() {
    this.dialogRef.close()
  }

  public getGenre() {
    if (!isNullOrUndefined(this.updateGenreView.id)) {
      this.updateGenreView.name = this.data.genres[this.updateGenreView.id].name
    }
  }

  public updateGenre() {
    if(isNullOrUndefined(this.updateGenreView.id)) {
      this.close()
      return
    }
    var temp: UpdateGenreViewItem =  new UpdateGenreViewItem()
    temp = this.data.genres[this.updateGenreView.id]
    if (!isNullOrUndefined(this.updateGenreView.name)) {
      temp.name = this.updateGenreView.name
    }
    this.genreService.updateGenre(temp).subscribe()
  this.close()
  }
}
