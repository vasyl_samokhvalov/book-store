import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GenreService } from 'src/shared/services/genre.service';

export interface DialogData {
}

@Component({
    selector: 'delete-genre-popup',
    templateUrl: 'delete-genre-popup.component.html',
    styleUrls: ['./delete-genre-popup.component.css']
})

export class DeleteGenrePopup {
    public deleteId: number

    constructor(
        public dialogRef: MatDialogRef<DeleteGenrePopup>,
        public genreService: GenreService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close()
    }

    public close(){
        this.dialogRef.close()
    }
    
    public deleteGenre() {
        this.genreService.deleteGenre(this.deleteId).subscribe()
        this.close()
    }
}
