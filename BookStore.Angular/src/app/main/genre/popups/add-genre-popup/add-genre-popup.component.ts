import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GenreService } from 'src/shared/services/genre.service';
import { GetAllGenresViewItem, AddGenreViewItem } from 'src/shared/models/genre/genre.view';

export interface DialogData {
    genres: GetAllGenresViewItem[]
}

@Component({
    selector: 'add-genre-popup',
    templateUrl: 'add-genre-popup.component.html',
    styleUrls: ['./add-genre-popup.component.css']
})

export class AddGenrePopup {
    public addGenreView: AddGenreViewItem = new AddGenreViewItem()
    public publisher
    public genre

    constructor(
        public dialogRef: MatDialogRef<AddGenrePopup>,
        private genreService: GenreService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close()
    }

    public close() {
        this.dialogRef.close()
    }

    public addGenre() {
        this.genreService.addGenre(this.addGenreView).subscribe()
        this.close()
    }
}
