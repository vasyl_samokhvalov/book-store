import { GetAllUsersView } from '../../shared/models/user/user.view';
import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../shared/services/login.service';
import { Token } from '../../shared/models/token/token.view';
import { CookieService } from 'ngx-cookie-service';
import { Router } from "@angular/router"
import { isNullOrUndefined } from 'util';
import { LoginGuard } from '../login-guard.component';

//const decodedToken = helper.decodedToken();

@Component({
  selector: 'login-app',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  public data: GetAllUsersView = new GetAllUsersView()
  private response: JSON
  token: Token = new Token()
  private loginString: string
  private passwordString: string

  constructor(private loginService: LoginService, private cookie: CookieService, private router: Router, private loginGuard: LoginGuard) { }

  ngOnInit() {
  }

  public login() {
    if (isNullOrUndefined(this.loginString) || isNullOrUndefined(this.passwordString)) {
      return
    }
    this.loginService.login(this.loginString, this.passwordString).subscribe(response => {
      this.response = JSON.parse(JSON.stringify(response))
      console.log(this.response)
      this.cookie.set("UserId", this.response["UserId"])
      this.cookie.set("UserName", this.response["UserName"])
      this.cookie.set("UserEmail", this.response["UserEmail"])
      this.cookie.set("AccessToken", this.response["AccessToken"])
      this.cookie.set("RefreshToken", this.response["RefreshToken"])
      this.cookie.set("UserRole", this.response["Role"])

      this.token.accessToken = this.response["AccessToken"]
      this.token.refreshToken = this.response["RefreshToken"]
      this.token.userEmail = this.response["UserEmail"]
      this.token.userName = this.response["UserName"]
      this.token.id = this.response["UserId"]
      window.location.replace('books')
    })
  }

  public getAll() {
    this.loginService.getAll().subscribe(data => { this.data = data; });
    console.log(this.data)
  }

  public checkTokens() {
    if (this.cookie.check("AccessToken")) {
      this.token.accessToken = this.cookie.get("AccessToken")
      this.token.refreshToken = this.cookie.get("RefreshToken")
      this.token.userEmail = this.cookie.get("UserEmail")
      this.token.userName = this.cookie.get("UserName")
      this.token.id = this.cookie.get("UserId")
    }
    this.loginService.checkTokens(this.token).subscribe(Role => {
    })
  }

  public logout() {
    this.loginService.logout().subscribe();
    this.cookie.deleteAll()
    localStorage.clear()
  }

  public getToken() {
    return this.token
  }
}