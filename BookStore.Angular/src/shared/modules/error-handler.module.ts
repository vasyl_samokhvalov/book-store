import { Injectable } from '@angular/core';
import {
    HttpEvent, HttpRequest, HttpHandler,
    HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { LoginComponent } from 'src/app/login/login.component';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private loginComponent: LoginComponent){}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            retry(1),
            catchError((error: HttpErrorResponse) => {
                if (error.status === 401) {
                    this.loginComponent.checkTokens()
                } else {
                    return throwError(error);
                }
            })
        );
    }
}