export class GetAllUsersView {
  public users: GetAllUsersViewItem[];

  constructor() {
    this.users = [];
  }
}

export class GetAllUsersViewItem {
  public id: number

  public name: string

  public password: string
}

export class UpdateUserViewItem {
  public id: number

  public name: string

  public password: string
}

export class DeleteUserViewItem {
  public id: number
}

export class RegisterViewItem {
  public email: string

  public name:string

  public password: string
}