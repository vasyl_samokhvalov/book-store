export class GetAllPublishersView {
    public publishers: GetAllPublishersViewItem[];

    constructor() {
        this.publishers = [];
    }
}

export class GetAllPublishersViewItem {
    public id: number;

    public name: string;
}

export class UpdatePublisherViewItem {
    public id: number

    public name: string;
}

export class DeletePublisherViewItem {
    public id: number
}

export class AddPublisherViewItem {
    public name: string;
}