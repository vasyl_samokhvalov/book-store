export class GetAllJournalsView {
    public journals: GetAllJournalsViewItem[];

    constructor() {
        this.journals = [];
    }
}

export class GetAllJournalsViewItem {
    public id: number

    public name: string

    public cost: number
}

export class UpdateJournalViewItem {
    public id: number

    public name: string

    public cost: number
}

export class DeleteJournalViewItem {
    public id: number
}

export class AddJournalViewItem {
    public name: string

    public cost: number;
}