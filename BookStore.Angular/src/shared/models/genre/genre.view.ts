export class GetAllGenresView {
    public genres: GetAllGenresViewItem[];

    constructor() {
        this.genres = [];
    }
}

export class GetAllGenresViewItem {
    public id: number

    public name: string
}

export class UpdateGenreViewItem {
    public id: number

    public name: string
}

export class DeleteGenreViewItem {
    public id: number
}

export class AddGenreViewItem {
    public name: string
}