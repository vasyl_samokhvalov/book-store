export class GetAllOrdersView {
    public orders: GetAllOrdersViewItem[];

    constructor() {
        this.orders = [];
    }
}

export class GetAllOrdersViewItem {
    public id: number
    
    public userId: string

    public cost: number
}

export class GetAllOrdersBigView {
    public orders: GetAllOrdersBigViewItem[];

    constructor() {
        this.orders = [];
    }
}

export class GetAllOrdersBigViewItem {
    public orderId: number

    public orderCost: number

    public bookId: number

    public bookName: string

    public bookCost: number
}

export class GetUserOrderView {
    public orders: GetUserOrderViewItem[];

    constructor() {
        this.orders = [];
    }
}

export class GetUserOrderViewItem {
    public id: number
    
    public userId: string

    public cost: number
}


export class UpdateOrderViewItem {
    public id: number

    public userId: string

    public cost: number
}

export class DeleteOrderViewItem {
    public id: number
}

export class AddOrderViewItem {
    public userId: string

    public cost: number
}