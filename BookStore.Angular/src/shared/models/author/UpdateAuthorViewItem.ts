export class UpdateAuthorViewItem {
  public id: number

  public name: string;

  public surname: string;
}