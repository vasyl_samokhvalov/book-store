export class AccessToken{
    public body:string
    public expires: Date
}

export class RefreshToken{
    public body:string
    public expires: Date
}

export class Token{
    public accessToken:string
    public refreshToken:string
    public userName: string
    public userEmail: string
    public id:string
    public role:string
}