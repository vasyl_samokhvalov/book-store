export class GetAllBooksView {
  public books: GetAllBooksViewItem[]

  constructor() {
    this.books = []
  }
}

export class GetAllBooksViewItem{
  public id: number

  public name: string

  public publisher: number

  public publisherName: string

  public cost: number

  public year: number

  public authors: number[] = []

  public authorsNames: string
}

export class UpdateBookViewItem{
  public id: number

  public name: string

  public cost: number

  public publisher: number

  public year: number
}

export class DeleteBookViewItem{
  public id: number
}

export class AddBookViewItem{
  public name: string

  public publisher: number

  public author: number[]

  public cost: number

  public year: number
}