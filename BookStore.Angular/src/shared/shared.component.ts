import { CookieService } from 'ngx-cookie-service';
import { NgModule } from '@angular/core';

@NgModule({
    imports:[CookieService],
    providers: [CookieService]
})
export class Shared {
    constructor(
    ) { }

}
