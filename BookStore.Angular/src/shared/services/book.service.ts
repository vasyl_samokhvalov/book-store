import { Observable } from 'rxjs';
import { GetAllBooksView, AddBookViewItem, DeleteBookViewItem, UpdateBookViewItem } from '../models/book/book.view';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class BookService {
    private url=environment.bookLink

    constructor(
        private http: HttpClient
    ) { }

    public getAll(): Observable<GetAllBooksView> {
        return this.http.get<GetAllBooksView>(this.url + "/GetAll")
    }

    public addBook(bookView: AddBookViewItem){
        return this.http.post(this.url + "/Create", bookView)
    }

    public updateBook(bookView: UpdateBookViewItem){
        return this.http.post(this.url +"/Update", bookView)
    }

    public deleteBook(id: number){
        var temp: DeleteBookViewItem ={
            id: id
        }
        return this.http.post(this.url +"/Delete", temp)
    }
}
