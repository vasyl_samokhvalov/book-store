import { User } from '../../app/main/entities/user';
import { Observable } from 'rxjs';
import { HttpClientModule, HttpClient, HttpParams } from '@angular/common/http';
import { GetAllUsersView, DeleteUserViewItem, UpdateUserViewItem, RegisterViewItem } from '../models/user/user.view';
import { GetAllUsersViewItem } from '../models/user/user.view'
import { Injectable } from '@angular/core';
import { LoginUserViewModel } from '../models/login/login.view';
import { Token } from '../models/token/token.view';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class LoginService {
    private url = environment.loginLink
    constructor(
        private http: HttpClient
    ) { }

    public login(email: string, password: string){
        var temp: LoginUserViewModel = new LoginUserViewModel()
        temp.Email = email//'qwerty@gmail.com'
        temp.Password = password//'qwerty'
        return this.http.post(this.url + "/Login", temp)
    }

    public getAll(): Observable<GetAllUsersView> {
        return this.http.get<GetAllUsersView>(this.url + "/GetAll");
    }

    public checkTokens(token: Token){
        var t: number = 1
        return this.http.post(this.url + "/CheckTokens", token)
    }

    public register(user: RegisterViewItem){
        return this.http.post(this.url + "/Register", user)
    }

    public logout(){
        return this.http.post(this.url + "/Logout", {})
    }
}
