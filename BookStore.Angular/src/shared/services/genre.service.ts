import { Genre } from '../../app/main/entities/genre';
import { Observable } from 'rxjs';
import { HttpClientModule, HttpClient, HttpParams } from '@angular/common/http';
import { GetAllGenresView, AddGenreViewItem, DeleteGenreViewItem, UpdateGenreViewItem } from '../models/genre/genre.view';
import { GetAllGenresViewItem } from '../models/genre/genre.view'
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class GenreService {
    private url=environment.genreLink

    constructor(
        private http: HttpClient
    ) { }

    public getAll(): Observable<GetAllGenresView> {
        return this.http.get<GetAllGenresView>(this.url + "/GetAll");
    }

    public addGenre(genreView: AddGenreViewItem){
        return this.http.post(this.url + "/Create", genreView)
    }

    public updateGenre(genreView: UpdateGenreViewItem){
        return this.http.post(this.url +"/Update", genreView);
    }

    public deleteGenre(id: number){
        var temp: DeleteGenreViewItem ={
            id: id
        }
        return this.http.post(this.url +"/Delete", temp);
    }
    
}
