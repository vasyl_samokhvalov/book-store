import { Publisher } from '../../app/main/entities/publisher';
import { Observable } from 'rxjs';
import { HttpClientModule, HttpClient, HttpParams } from '@angular/common/http';
import { GetAllPublishersView, AddPublisherViewItem, DeletePublisherViewItem, UpdatePublisherViewItem } from '../models/publisher/publisher.view';
import { GetAllPublishersViewItem } from '../models/publisher/publisher.view'
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class PublisherService {
    private url=environment.publicherLink

    constructor(
        private http: HttpClient
    ) { }

    public getAll(): Observable<GetAllPublishersView> {
        return this.http.get<GetAllPublishersView>(this.url + "/GetAll");
    }

    public addPublisher(publisherView: AddPublisherViewItem){
        return this.http.post(this.url + "/Create", publisherView)
    }

    public updatePublisher(publisherView: UpdatePublisherViewItem){
        return this.http.post(this.url +"/Update", publisherView);
    }

    public deletePublisher(id: number){
        var temp: DeletePublisherViewItem ={
            id: id
        }
        return this.http.post(this.url +"/Delete", temp);
    }
    
}
