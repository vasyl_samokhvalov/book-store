import { BookAuthor } from '../../app/main/entities/book-author';
import { Observable } from 'rxjs';
import { HttpClientModule, HttpClient, HttpParams } from '@angular/common/http';
import { GetAllBookAuthorsView, AddBookAuthorViewItem, DeleteBookAuthorViewItem, UpdateBookAuthorViewItem } from '../models/book-author/book-author.view';
import { GetAllBookAuthorsViewItem } from '../models/book-author/book-author.view'
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class BookAuthorService {
    private url="https://localhost:44342/BookAuthor"

    constructor(
        private http: HttpClient
    ) { }

    public getAll(): Observable<GetAllBookAuthorsView> {
        return this.http.get<GetAllBookAuthorsView>(this.url + "/GetAll");
    }

    public addBookAuthor(bookauthorView: AddBookAuthorViewItem){
        return this.http.post(this.url + "/Create", bookauthorView)
    }

    public updateBookAuthor(bookauthorView: UpdateBookAuthorViewItem){
        return this.http.put(this.url +"/Update", bookauthorView);
    }

    public deleteBookAuthor(id: number){
        var temp: DeleteBookAuthorViewItem ={
            id: id
        }
        return this.http.post(this.url +"/Delete", temp);
    }
    
}
