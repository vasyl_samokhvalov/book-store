import { Observable } from 'rxjs';
import { HttpClient} from '@angular/common/http';
import { GetAllUsersView, DeleteUserViewItem, UpdateUserViewItem, RegisterViewItem } from '../models/user/user.view';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    private url=environment.userLink

    constructor(
        private http: HttpClient
    ) { }

    public getAll(): Observable<GetAllUsersView> {
        return this.http.get<GetAllUsersView>(this.url + "/GetAll");
    }

    //NOT IMPLEMENTED
    public addUser(userView: RegisterViewItem){
        return this.http.post(this.url + "/Create", userView)
    }
    //--
    public updateUser(userView: UpdateUserViewItem){
        return this.http.post(this.url +"/Update", userView);
    }
    //--
    public deleteUser(id: number){
        var temp: DeleteUserViewItem ={
            id: id
        }
        return this.http.post(this.url +"/Delete", temp);
    }
}
