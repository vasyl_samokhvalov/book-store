import { Observable } from 'rxjs';
import { GetAllAuthorsView} from '../models/author/GetAllAuthorsView';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { AddAuthorViewItem } from '../models/author/AddAuthorViewItem';
import { UpdateAuthorViewItem } from '../models/author/UpdateAuthorViewItem';
import { DeleteAuthorViewItem } from '../models/author/DeleteAuthorViewItem';

@Injectable({
    providedIn: 'root'
})
export class AuthorService {
    private url=environment.authorLink
    constructor(
        private http: HttpClient
    ) { }

    public getAll(): Observable<GetAllAuthorsView> {
        return this.http.get<GetAllAuthorsView>(this.url + "/GetAll");
    }

    public addAuthor(authorView: AddAuthorViewItem){
        return this.http.post(this.url + "/Create", authorView)
    }

    public updateAuthor(authorView: UpdateAuthorViewItem){
        return this.http.post(this.url +"/Update", authorView);
    }

    public deleteAuthor(id: number){
        var temp: DeleteAuthorViewItem ={
            id: id
        }

        return this.http.post(this.url +"/Delete", temp);
    }
    
}
