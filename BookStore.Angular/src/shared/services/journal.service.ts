
import { Observable } from 'rxjs';
import { HttpClient} from '@angular/common/http';
import { GetAllJournalsView, AddJournalViewItem, DeleteJournalViewItem, UpdateJournalViewItem } from '../models/journal/journal.view';
import { GetAllJournalsViewItem } from '../models/journal/journal.view'
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class JournalService {
    private url=environment.journalLink

    constructor(
        private http: HttpClient
    ) { }

    public getAll(): Observable<GetAllJournalsView> {
        return this.http.get<GetAllJournalsView>(this.url + "/GetAll");
    }

    public addJournal(journalView: AddJournalViewItem){
        return this.http.post(this.url + "/Create", journalView)
    }

    public updateJournal(journalView: UpdateJournalViewItem){
        return this.http.post(this.url +"/Update", journalView);
    }

    public deleteJournal(id: number){
        var temp: DeleteJournalViewItem ={
            id: id
        }
        return this.http.post(this.url +"/Delete", temp);
    }
    
}
