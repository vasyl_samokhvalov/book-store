import { Order } from '../../app/main/entities/order';
import { Observable } from 'rxjs';
import { HttpClientModule, HttpClient, HttpParams } from '@angular/common/http';
import { GetAllOrdersView, AddOrderViewItem, DeleteOrderViewItem, UpdateOrderViewItem, GetAllOrdersBigView, GetUserOrderView } from '../models/order/order.view';
import { GetAllOrdersViewItem } from '../models/order/order.view'
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UserOrder } from 'src/app/main/entities/user-order';

@Injectable({
    providedIn: 'root'
})
export class OrderService {
    private url=environment.orderLink

    constructor(
        private http: HttpClient
    ) { }

    public getAll(): Observable<GetAllOrdersView> {
        return this.http.get<GetAllOrdersView>(this.url + "/GetAll");
    }

    public getAllBig(): Observable<GetAllOrdersBigView> {
        return this.http.get<GetAllOrdersBigView>(this.url + "/GetAllBig");
    }

    public addOrder(orderView: AddOrderViewItem){
        return this.http.post(this.url + "/Create", orderView)
    }

    public updateOrder(orderView: UpdateOrderViewItem){
        return this.http.post(this.url +"/Update", orderView);
    }

    public getUserOrder(userId: string): Observable<UserOrder>{
        let params = new HttpParams().set("userId", userId)
        return this.http.get<UserOrder>(this.url + "/GetUserOrder", {params: params})
    }

    public deleteOrder(id: number){
        var temp: DeleteOrderViewItem ={
            id: id
        }
        return this.http.post(this.url +"/Delete", temp);
    }
    
}
