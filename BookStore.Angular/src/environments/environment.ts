// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  authorLink: 'https://localhost:44367/Author',
  bookLink: 'https://localhost:44367/Book',
  genreLink: 'https://localhost:44367/Genre',
  journalLink: 'https://localhost:44367/Journal',
  loginLink: 'https://localhost:44367/Account',
  orderLink: 'https://localhost:44367/Order',
  publicherLink: 'https://localhost:44367/Publisher',
  userLink: 'https://localhost:44367/Users'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
