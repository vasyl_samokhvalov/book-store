﻿using AutoMapper;
using BookStore.BusinessLogic;
using BookStore.DataAccess.Entities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using BookStore.DataAccess.DapperRepositories;
using BookStore.DataAccess.RepositoryInterfaces;

namespace BookStore.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.RequireHttpsMetadata = false;
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            // укзывает, будет ли валидироваться издатель при валидации токена
                            ValidateIssuer = true,
                            // строка, представляющая издателя
                            ValidIssuer = AuthenticationOptions.ISSUER,

                            // будет ли валидироваться потребитель токена
                            ValidateAudience = true,
                            // установка потребителя токена
                            ValidAudience = AuthenticationOptions.AUDIENCE,
                            // будет ли валидироваться время существования
                            ValidateLifetime = true,

                            // установка ключа безопасности
                            IssuerSigningKey = AuthenticationOptions.GetSymmetricSecurityKey(),
                            // валидация ключа безопасности
                            ValidateIssuerSigningKey = true,
                        };
                    });

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddCors(options =>
            {
                options.AddDefaultPolicy(builder =>
                {
                    builder.AllowAnyHeader()
                           .AllowAnyMethod()
                           .AllowAnyOrigin();
                });
            });
            services.AddTransient<DataAccess.RepositoryInterfaces.IAuthorRepository, DataAccess.DapperRepositories.AuthorRepository>(provider => new DataAccess.DapperRepositories.AuthorRepository("DefaultConnection"));
            services.AddTransient<DataAccess.RepositoryInterfaces.IBookRepository, DataAccess.DapperRepositories.BookRepository>(provider => new DataAccess.DapperRepositories.BookRepository("DefaultConnection"));
            services.AddTransient<DataAccess.RepositoryInterfaces.IBookAuthorRepository, DataAccess.DapperRepositories.BookAuthorRepository>(provider => new DataAccess.DapperRepositories.BookAuthorRepository("DefaultConnection"));
            services.AddTransient<DataAccess.RepositoryInterfaces.IBookGenreRepository, DataAccess.DapperRepositories.BookGenreRepository>(provider => new DataAccess.DapperRepositories.BookGenreRepository("DefaultConnection"));
            services.AddTransient<DataAccess.RepositoryInterfaces.IBookJournalRepository, DataAccess.DapperRepositories.BookJournalRepository>(provider => new DataAccess.DapperRepositories.BookJournalRepository("DefaultConnection"));
            services.AddTransient<DataAccess.RepositoryInterfaces.IBookOrderRepository, DataAccess.DapperRepositories.BookOrderRepository>(provider => new DataAccess.DapperRepositories.BookOrderRepository("DefaultConnection"));
            services.AddTransient<DataAccess.RepositoryInterfaces.IGenreRepository, DataAccess.DapperRepositories.GenreRepository>(provider => new DataAccess.DapperRepositories.GenreRepository("DefaultConnection"));
            services.AddTransient<DataAccess.RepositoryInterfaces.IJournalRepository, DataAccess.DapperRepositories.JournalRepository>(provider => new DataAccess.DapperRepositories.JournalRepository("DefaultConnection"));
            services.AddTransient<DataAccess.RepositoryInterfaces.IJournalOrderRepository, DataAccess.DapperRepositories.JournalOrderRepository>(provider => new DataAccess.DapperRepositories.JournalOrderRepository("DefaultConnection"));
            services.AddTransient<DataAccess.RepositoryInterfaces.IOrderRepository, DataAccess.DapperRepositories.OrderRepository>(provider => new DataAccess.DapperRepositories.OrderRepository("DefaultConnection"));
            services.AddTransient<DataAccess.RepositoryInterfaces.IPublisherRepository, DataAccess.DapperRepositories.PublisherRepository>(provider => new DataAccess.DapperRepositories.PublisherRepository("DefaultConnection"));

            services.AddTransient<BusinessLogic.Services.IAccountService, BusinessLogic.Services.AccountService>();
            services.AddTransient<BusinessLogic.Services.IUserService, BusinessLogic.Services.UserService>();
            services.AddTransient<BusinessLogic.Services.IAuthorService, BusinessLogic.Services.AuthorService>();
            services.AddTransient<BusinessLogic.Services.IBookService, BusinessLogic.Services.BookService>();
            services.AddTransient<BusinessLogic.Services.IGenreService, BusinessLogic.Services.GenreService>();
            services.AddTransient<BusinessLogic.Services.IJournalService, BusinessLogic.Services.JournalService>();
            services.AddTransient<BusinessLogic.Services.IOrderService, BusinessLogic.Services.OrderService>();
            services.AddTransient<BusinessLogic.Services.IPublisherService, BusinessLogic.Services.PublisherService>();


            //validator
            //services.AddTransient<IPasswordValidator<User>, CustomPasswordValidator>(serv => new CustomPasswordValidator(6));
            //services.AddTransient<IUserValidator<User>, CustomUserValidator>(serv => new CustomUserValidator());

            services.AddDbContext<DataAccess.ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("BookStore.DataAccess")));

            services.AddIdentity<User, IdentityRole>(opts =>
            {
                //for login
                opts.User.RequireUniqueEmail = true;
                opts.User.AllowedUserNameCharacters = default;
                //for password
                opts.Password.RequiredLength = 5;
                opts.Password.RequireNonAlphanumeric = false;
                opts.Password.RequireLowercase = false;
                opts.Password.RequireUppercase = false;
                opts.Password.RequireDigit = false;
            })
                .AddEntityFrameworkStores<DataAccess.ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddScoped<IDbConnection, SqlConnection>(c => new SqlConnection("DefaultConnection"));

            services.AddAutoMapper();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddFile(Path.Combine(Directory.GetCurrentDirectory(), "logger.txt"));
            string fgf = Path.Combine(Directory.GetCurrentDirectory(), "logger.txt");
            var logger = loggerFactory.CreateLogger("Logger.txt");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();

            app.UseCors(builder => builder.AllowAnyOrigin()
                                          .AllowAnyHeader()
                                          .AllowAnyMethod());
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "");//{controller=Home}/{action=Index}/{id?}
            });
            app.Run(async (context) =>
            {
                logger.LogInformation("Processing request {0}", context.Request.Path);
                await context.Response.WriteAsync("BookStore online...");
            });
        }

    }
}
