﻿using AutoMapper;
using BookStore.BusinessLogic.Views;
using BookStore.DataAccess.Entities;

namespace BookStore.WebAPI.Profiles
{
    public class MapProfiles : Profile
    {
        public MapProfiles()
        {
            CreateMap<Book, GetAllBooksViewItem>();
            CreateMap<GetAllBooksViewItem, Book>();
            CreateMap<Book, CreateBookView>();
            CreateMap<CreateBookView, Book>();
            CreateMap<UpdateBookView, Book>();
            CreateMap<Book, UpdateBookView>();

            CreateMap<Author, GetAllAuthorsViewItem>();
            CreateMap<GetAllAuthorsViewItem, Author>();
            CreateMap<Author, CreateAuthorView>();
            CreateMap<CreateAuthorView, Author>();
            CreateMap<UpdateAuthorView, Author>();
            CreateMap<Author, UpdateAuthorView>();


            CreateMap<Genre, GetAllGenresViewItem>();
            CreateMap<GetAllGenresViewItem, Genre>();
            CreateMap<Genre, CreateGenreView>();
            CreateMap<CreateGenreView, Genre>();
            CreateMap<UpdateGenreView, Genre>();
            CreateMap<Genre, UpdateGenreView>();

            CreateMap<Journal, GetAllJournalsViewItem>();
            CreateMap<GetAllJournalsViewItem, Journal>();
            CreateMap<Journal, CreateJournalView>();
            CreateMap<CreateJournalView, Journal>();
            CreateMap<UpdateJournalView, Journal>();
            CreateMap<Journal, UpdateJournalView>();

            CreateMap<Order, GetAllOrdersViewItem>();
            CreateMap<GetAllOrdersViewItem, Order>();
            CreateMap<Order, CreateOrderView>();
            CreateMap<CreateOrderView, Order>();
            CreateMap<UpdateOrderView, Order>();
            CreateMap<Order, UpdateOrderView>();
            CreateMap<Order, GetUserOrderView>();
            CreateMap<GetUserOrderView, Order>();
            CreateMap<UserOrder, GetUserOrderViewItem>();
            CreateMap<GetUserOrderView, UserOrder>();

            CreateMap<Publisher, GetAllPublishersViewItem>();
            CreateMap<GetAllPublishersViewItem, Publisher>();
            CreateMap<Publisher, CreatePublisherView>();
            CreateMap<CreatePublisherView, Publisher>();
            CreateMap<UpdatePublisherView, Publisher>();
            CreateMap<Publisher, UpdatePublisherView>();

            CreateMap<User, GetAllUsersViewItem>();
            CreateMap<GetAllUsersViewItem, User>();
            CreateMap<UpdateUserView, User>();
            CreateMap<User, UpdateUserView>();
            CreateMap<LoginUserView, User>();
            CreateMap<User, LoginUserView>();
        }
    }
}
