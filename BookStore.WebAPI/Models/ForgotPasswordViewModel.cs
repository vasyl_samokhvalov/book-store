﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.WebAPI.Models
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
