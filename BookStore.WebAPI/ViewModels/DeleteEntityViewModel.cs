﻿namespace BookStore.WebAPI.ViewModels
{
    public class DeleteEntityViewModel
    {
        public int Id { get; set; }
    }
}
