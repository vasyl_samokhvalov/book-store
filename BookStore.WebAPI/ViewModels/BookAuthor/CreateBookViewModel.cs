﻿using BookStore.DataAccess.Entities;


namespace BookStore.WebAPI.ViewModels
{
    public class CreateBookAuthorViewModel
    {
        public int BookId { get; set; }
        public Book Book { get; set; }
        public int AuthorId { get; set; }
        public Author Author { get; set; }
    }
}
