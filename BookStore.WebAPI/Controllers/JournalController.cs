﻿using BookStore.BusinessLogic.Services;
using BookStore.BusinessLogic.Views;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.WebAPI.Controllers
{
    [Route("[controller]/[action]")]

    public class JournalController : Controller
    {
        private readonly IJournalService _service;

        public JournalController(IJournalService journalService)
        {
            _service = journalService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_service.GetAll());
        }

        [HttpPost]
        public IActionResult Create([FromBody]CreateJournalView item)
        {
            _service.Create(item);
            return Ok();
        }

        [HttpPost]
        public IActionResult Update([FromBody]UpdateJournalView item)
        {
            _service.Update(item);

            return Ok();
        }

        [HttpPost]
        public IActionResult Delete([FromBody]DeleteEntityView item)
        {
            _service.Delete(item.Id);
            return Ok();
        }
    }
}