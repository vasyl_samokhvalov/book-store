﻿using BookStore.BusinessLogic.Services;
using BookStore.BusinessLogic.Views;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace BookStore.WebAPI.Controllers
{
    [Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly IAccountService _accountService;
        private readonly IUserService _userService;

        public AccountController(IAccountService service)
        {
            _accountService = service;
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody]LoginUserView item)
        {
            return Ok(await _accountService.Login(item));
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody]RegisterView item)
        {
            var result = await _userService.Create(item);
            if (result.Succeeded)
            {
                var code = await _accountService.GenerateEmailConfirmationTokenAsync(item);
                var callbackUrl = Url.Action(
                    "ConfirmEmail",
                    "Account",
                    new { userId = _userService.GetUserId(item), code = code },
                    protocol: HttpContext.Request.Scheme);
                EmailHelper emailHelper = new EmailHelper();
                await emailHelper.SendEmailAsync(
                    item.Email,
                    "Confrim your BookStore account!",
                    $"Confirm your registration by following the link: <a href=\""
                                                       + callbackUrl + "\">Confirm registration</a>");

                return Content("For confirming the registrations please check your e-mail and follow the link!");
            }

            return BadRequest();
        }

        [HttpPost]
        public Task<TokenView> CheckTokens([FromBody]TokenView token)
        {
            return _accountService.CheckTokens(token);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public void Logout()
        {
            _accountService.Logout();
        }

        [HttpGet]
        [AllowAnonymous]
        public async void ConfirmEmail(string userId, string code)
        {
            await _accountService.ConfirmEmail(userId, code);
        }

        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var user = await _userManager.FindByNameAsync(model.Email);
        //        if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
        //        {
        //            // пользователь с данным email может отсутствовать в бд
        //            // тем не менее мы выводим стандартное сообщение, чтобы скрыть 
        //            // наличие или отсутствие пользователя в бд
        //            return View("ForgotPasswordConfirmation");
        //        }

        //        var code = await _userManager.GeneratePasswordResetTokenAsync(user);
        //        var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
        //        EmailService emailService = new EmailService();
        //        await emailService.SendEmailAsync(model.Email, "Reset Password",
        //            $"Для сброса пароля пройдите по ссылке: <a href='{callbackUrl}'>link</a>");
        //        return View("ForgotPasswordConfirmation");
        //    }
        //    return View(model);
        //}

        //[HttpGet]
        //[AllowAnonymous]
        //public IActionResult ResetPassword(string code = null)
        //{
        //    return code == null ? View("Error") : View();
        //}

        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }
        //    var user = await _userManager.FindByNameAsync(model.Email);
        //    if (user == null)
        //    {
        //        return View("ResetPasswordConfirmation");
        //    }
        //    var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
        //    if (result.Succeeded)
        //    {
        //        return View("ResetPasswordConfirmation");
        //    }
        //    foreach (var error in result.Errors)
        //    {
        //        ModelState.AddModelError(string.Empty, error.Description);
        //    }
        //    return View(model);
        //}
    }
}