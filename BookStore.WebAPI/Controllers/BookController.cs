﻿using BookStore.BusinessLogic.Services;
using BookStore.BusinessLogic.Views;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookStore.WebAPI.Controllers
{

    [Route("[controller]/[action]")]
    public class BookController : Controller
    {
        private readonly IBookService _service;

        public BookController(IBookService service)
        {
            _service = service;
        }
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _service.GetAll());
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody]CreateBookView item)
        {
            await _service.Create(item);
            return Ok();
        }

        [HttpPost]
        public IActionResult Update([FromBody]UpdateBookView item)
        {
            _service.Update(item);
            return Ok();
        }

        [HttpPost]
        public IActionResult Delete([FromBody]DeleteEntityView item)
        {
            _service.Delete(item.Id);
            return Ok();
        }

        [HttpPost]
        public IActionResult GetBasket([FromBody]string idString)
        {
            return Ok(_service.GetAll());
        }
    }
}