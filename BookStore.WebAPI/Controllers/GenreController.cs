﻿using BookStore.BusinessLogic.Services;
using BookStore.BusinessLogic.Views;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.WebAPI.Controllers
{
    [Route("[controller]/[action]")]

    public class GenreController : Controller
    {
        private readonly IGenreService _service;

        public GenreController(IGenreService service)
        {
            _service = service;
        }

        public IActionResult GetAll()
        {
            return Ok(_service.GetAll());
        }

        [HttpPost]
        public IActionResult Create([FromBody]CreateGenreView item)
        {
            _service.Create(item);
            return Ok();
        }

        [HttpPost]
        public IActionResult Update([FromBody]UpdateGenreView item)
        {
            _service.Update(item);
            return Ok();
        }

        [HttpPost]
        public IActionResult Delete([FromBody]DeleteEntityView item)
        {
            _service.Delete(item.Id);
            return Ok();
        }
    }
}