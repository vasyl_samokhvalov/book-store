﻿using BookStore.BusinessLogic.Services;
using BookStore.BusinessLogic.Views;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.WebAPI.Controllers
{
    [Route("[controller]/[action]")]

    public class OrderController : Controller
    {
        private readonly IOrderService _service;

        public OrderController(IOrderService orderService)
        {
            _service = orderService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_service.GetAll());
        }

        [HttpPost]
        public IActionResult Create([FromBody]CreateOrderView item)
        {
            _service.Create(item);
            return Ok();
        }

        [HttpPost]
        public IActionResult Update([FromBody]UpdateOrderView item)
        {
            _service.Update(item);

            return Ok();
        }

        [HttpPost]
        public IActionResult Delete([FromBody]DeleteEntityView item)
        {
            _service.Delete(item.Id);
            return Ok();
        }

        [HttpGet]
        public IActionResult GetUserOrder(string userId)
        {
            return Ok(_service.GetUserOrder(userId));
        }
    }
}