﻿using BookStore.BusinessLogic.Services;
using BookStore.BusinessLogic.Views;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.WebAPI.Controllers
{
    [Route("[controller]/[action]")]

    public class AuthorController : Controller
    {
        private readonly IAuthorService _service;

        public AuthorController(IAuthorService authorService)
        {
            _service = authorService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_service.GetAll());
        }

        [HttpPost]
        public IActionResult Create([FromBody]CreateAuthorView item)
        {
            _service.Create(item);
            return Ok();
        }

        [HttpPost]
        public IActionResult Update([FromBody]UpdateAuthorView item)
        {
            _service.Update(item);
            return Ok();
        }

        [HttpPost]
        public IActionResult Delete([FromBody]DeleteEntityView item)
        {
            _service.Delete(item.Id);
            return Ok();
        }
    }
}