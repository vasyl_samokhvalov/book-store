﻿using BookStore.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace BookStore.WebAPI.Controllers
{
    //[Route("[contoller]/[action]")]
    public class HomeController : Controller
    {

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
