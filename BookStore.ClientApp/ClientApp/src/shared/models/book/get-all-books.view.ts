export class GetAllBooksView {
  public books: GetAllBooksViewItem[]

  constructor() {
    this.books = []
  }
}

export class GetAllBooksViewItem{
  public id: number

  public name: string

  public publisher: number

  public publisherName: string

  public cost: number

  public year: number

  public authors: number[] = []

  public authorsNames: string
}
