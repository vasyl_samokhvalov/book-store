export class AddBookViewItem{
  public name: string

  public publisher: number

  public author: number[]

  public cost: number

  public year: number
}
