export class UpdateBookViewItem{
  public id: number

  public name: string

  public cost: number

  public publisher: number

  public year: number
}
