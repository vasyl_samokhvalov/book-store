export class GetAllUsersView {
  public users: GetAllUsersViewItem[];

  constructor() {
    this.users = [];
  }
}

export class GetAllUsersViewItem {
  public id: number

  public name: string

  public password: string
}