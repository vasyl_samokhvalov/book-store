import { Book } from '../../../app/main/entities/book';
import { Author } from '../../../app/main/entities/author';

export class GetAllBookAuthorsView {
    public bookauthors: GetAllBookAuthorsViewItem[];
  
    constructor() {
      this.bookauthors = [];
    }
  }
  
  export class GetAllBookAuthorsViewItem {
    public id: number

    public book: Book

    public author: Author
  }