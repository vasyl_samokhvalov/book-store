import { Book } from '../../../app/main/entities/book';
import { Author } from '../../../app/main/entities/author';

export class UpdateBookAuthorViewItem {
  public id: number

  public book: Book

  public author: Author
}