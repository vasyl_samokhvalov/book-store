export class GetAllPublishersView {
    public publishers: GetAllPublishersViewItem[];

    constructor() {
        this.publishers = [];
    }
}

export class GetAllPublishersViewItem {
    public id: number;

    public name: string;
}