export class GetAllGenresView {
    public genres: GetAllGenresViewItem[];

    constructor() {
        this.genres = [];
    }
}

export class GetAllGenresViewItem {
    public id: number

    public name: string
}