export class GetAllAuthorsView {
  public authors: GetAllAuthorsViewItem[];

  constructor() {
    this.authors = [];
  }
}

export class GetAllAuthorsViewItem {
  public id: number;

  public name: string;

  public surname: string;
}