export class GetAllJournalsView {
    public journals: GetAllJournalsViewItem[];

    constructor() {
        this.journals = [];
    }
}

export class GetAllJournalsViewItem {
    public id: number

    public name: string

    public cost: number
}