export class UpdateOrderViewItem {
    public id: number

    public userId: string

    public cost: number
}