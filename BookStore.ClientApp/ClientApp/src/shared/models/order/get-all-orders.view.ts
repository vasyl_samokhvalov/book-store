export class GetAllOrdersView {
    public orders: GetAllOrdersViewItem[];

    constructor() {
        this.orders = [];
    }
}

export class GetAllOrdersViewItem {
    public id: number
    
    public userId: string

    public cost: number
}