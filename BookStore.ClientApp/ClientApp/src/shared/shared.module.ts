import { CookieService } from 'ngx-cookie-service';
import { NgModule } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ErrorInterceptor } from './modules/error-handler.module';

@NgModule({
    imports:[ErrorInterceptor],
    providers: [CookieService],
    exports: [ToastrService, CookieService],
    declarations: [ToastrService, CookieService]
})
export class Shared {
    constructor(
    ) { }

}
