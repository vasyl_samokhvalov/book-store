import { Observable } from 'rxjs';
import {  HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GetAllBookAuthorsView } from '../models/book-author/get-all-book-authors.view';
import { AddBookAuthorViewItem } from '../models/book-author/add-book-author.view';
import { UpdateBookAuthorViewItem } from '../models/book-author/update-book-author.view';
import { DeleteBookAuthorViewItem } from '../models/book-author/delete-book-author.view';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class BookAuthorService {
    private url=environment.APIUrl + 'BookAuthor'

    constructor(
        private http: HttpClient
    ) { }

    public getAll(): Observable<GetAllBookAuthorsView> {
        return this.http.get<GetAllBookAuthorsView>(this.url + "/GetAll");
    }

    public addBookAuthor(bookauthorView: AddBookAuthorViewItem){
        return this.http.post(this.url + "/Create", bookauthorView)
    }

    public updateBookAuthor(bookauthorView: UpdateBookAuthorViewItem){
        return this.http.put(this.url +"/Update", bookauthorView);
    }

    public deleteBookAuthor(id: number){
        var temp: DeleteBookAuthorViewItem ={
            id: id
        }
        return this.http.post(this.url +"/Delete", temp);
    }
    
}
