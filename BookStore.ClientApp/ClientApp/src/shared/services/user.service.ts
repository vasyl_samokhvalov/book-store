import { Observable } from 'rxjs';
import { HttpClient} from '@angular/common/http';
import { GetAllUsersView } from '../models/user/get-all-users.view';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { RegisterViewItem } from '../models/user/register-user.view';
import { UpdateUserViewItem } from '../models/user/update-user.view';
import { DeleteUserViewItem } from '../models/user/delete-user.view';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    private url=environment.APIUrl + 'User'

    constructor(
        private http: HttpClient
    ) { }

    public getAll(): Observable<GetAllUsersView> {
        return this.http.get<GetAllUsersView>(this.url + "/GetAll");
    }

    //NOT IMPLEMENTED
    public addUser(userView: RegisterViewItem){
        return this.http.post(this.url + "/Create", userView)
    }
    //--
    public updateUser(userView: UpdateUserViewItem){
        return this.http.post(this.url +"/Update", userView);
    }
    //--
    public deleteUser(id: number){
        var temp: DeleteUserViewItem ={
            id: id
        }
        return this.http.post(this.url +"/Delete", temp);
    }
}
