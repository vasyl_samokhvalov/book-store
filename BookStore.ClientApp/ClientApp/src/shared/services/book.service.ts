import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { GetAllBooksView } from '../models/book/get-all-books.view';
import { UpdateBookViewItem } from '../models/book/update-book.view';
import { DeleteBookViewItem } from '../models/book/delete-book.view';
import { AddBookViewItem } from '../models/book/add-book.view';

@Injectable({
    providedIn: 'root'
})
export class BookService {
    private url=environment.APIUrl + 'Book'

    constructor(
        private http: HttpClient
    ) { }

    public getAll(): Observable<GetAllBooksView> {
        return this.http.get<GetAllBooksView>(this.url + "/GetAll")
    }

    public addBook(bookView: AddBookViewItem){
        return this.http.post(this.url + "/Create", bookView)
    }

    public updateBook(bookView: UpdateBookViewItem){
        return this.http.post(this.url +"/Update", bookView)
    }

    public deleteBook(id: number){
        var temp: DeleteBookViewItem ={
            id: id
        }
        return this.http.post(this.url +"/Delete", temp)
    }
}
