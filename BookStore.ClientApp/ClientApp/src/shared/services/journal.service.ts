
import { Observable } from 'rxjs';
import { HttpClient} from '@angular/common/http';
import { GetAllJournalsView } from '../models/journal/get-all-journals.view';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AddJournalViewItem } from '../models/journal/add-journal.view';
import { UpdateJournalViewItem } from '../models/journal/update-journal.view';
import { DeleteJournalViewItem } from '../models/journal/delete-journal.view';

@Injectable({
    providedIn: 'root'
})
export class JournalService {
    private url=environment.APIUrl + 'Journal'

    constructor(
        private http: HttpClient
    ) { }

    public getAll(): Observable<GetAllJournalsView> {
        return this.http.get<GetAllJournalsView>(this.url + "/GetAll");
    }

    public addJournal(journalView: AddJournalViewItem){
        return this.http.post(this.url + "/Create", journalView)
    }

    public updateJournal(journalView: UpdateJournalViewItem){
        return this.http.post(this.url +"/Update", journalView);
    }

    public deleteJournal(id: number){
        var temp: DeleteJournalViewItem ={
            id: id
        }
        return this.http.post(this.url +"/Delete", temp);
    }
    
}
