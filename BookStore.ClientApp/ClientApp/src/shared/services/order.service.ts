import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UserOrder } from 'src/app/main/entities/user-order';
import { GetAllOrdersView } from '../models/order/get-all-orders.view';
import { AddOrderViewItem } from '../models/order/add-order-view';
import { UpdateOrderViewItem } from '../models/order/update-order.view';
import { DeleteOrderViewItem } from '../models/order/delete-order.view';

@Injectable({
    providedIn: 'root'
})
export class OrderService {
    private url=environment.APIUrl + 'Order'

    constructor(
        private http: HttpClient
    ) { }

    public getAll(): Observable<GetAllOrdersView> {
        return this.http.get<GetAllOrdersView>(this.url + "/GetAll");
    }

    public addOrder(orderView: AddOrderViewItem){
        return this.http.post(this.url + "/Create", orderView)
    }

    public updateOrder(orderView: UpdateOrderViewItem){
        return this.http.post(this.url +"/Update", orderView);
    }

    public getUserOrder(userId: string): Observable<UserOrder>{
        let params = new HttpParams().set("userId", userId)
        return this.http.get<UserOrder>(this.url + "/GetUserOrder", {params: params})
    }

    public deleteOrder(id: number){
        var temp: DeleteOrderViewItem ={
            id: id
        }
        return this.http.post(this.url +"/Delete", temp);
    }
    
}
