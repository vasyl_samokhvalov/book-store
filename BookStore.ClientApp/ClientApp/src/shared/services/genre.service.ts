import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GetAllGenresView } from '../models/genre/get-all-genres.view';
import { AddGenreViewItem } from '../models/genre/add-genre-view';
import { UpdateGenreViewItem } from '../models/genre/update-genre.view';
import { DeleteGenreViewItem } from '../models/genre/delete-genre-view';

@Injectable({
    providedIn: 'root'
})
export class GenreService {
    private url=environment.APIUrl + 'Genre'

    constructor(
        private http: HttpClient
    ) { }

    public getAll(): Observable<GetAllGenresView> {
        return this.http.get<GetAllGenresView>(this.url + "/GetAll");
    }

    public addGenre(genreView: AddGenreViewItem){
        return this.http.post(this.url + "/Create", genreView)
    }

    public updateGenre(genreView: UpdateGenreViewItem){
        return this.http.post(this.url +"/Update", genreView);
    }

    public deleteGenre(id: number){
        var temp: DeleteGenreViewItem ={
            id: id
        }
        return this.http.post(this.url +"/Delete", temp);
    }
    
}
