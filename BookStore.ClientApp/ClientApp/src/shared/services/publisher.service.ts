import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GetAllPublishersView } from '../models/publisher/get-all-publishers.view';
import { AddPublisherViewItem } from '../models/publisher/add-publisher.view';
import { UpdatePublisherViewItem } from '../models/publisher/update-publisher.view';
import { DeletePublisherViewItem } from '../models/publisher/delete-publisher.view';

@Injectable({
    providedIn: 'root'
})
export class PublisherService {
    private url=environment.APIUrl + 'Publisher'

    constructor(
        private http: HttpClient
    ) { }

    public getAll(): Observable<GetAllPublishersView> {
        return this.http.get<GetAllPublishersView>(this.url + "/GetAll");
    }

    public addPublisher(publisherView: AddPublisherViewItem){
        return this.http.post(this.url + "/Create", publisherView)
    }

    public updatePublisher(publisherView: UpdatePublisherViewItem){
        return this.http.post(this.url +"/Update", publisherView);
    }

    public deletePublisher(id: number){
        var temp: DeletePublisherViewItem ={
            id: id
        }
        return this.http.post(this.url +"/Delete", temp);
    }
    
}
