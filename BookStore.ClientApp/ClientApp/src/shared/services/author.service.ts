import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { AddAuthorViewItem } from '../models/author/add-author.view';
import { GetAllAuthorsView } from '../models/author/get-all-authors.view.';
import { UpdateAuthorViewItem } from '../models/author/update-author.view';
import { DeleteAuthorViewItem } from '../models/author/delte-author.view';

@Injectable({
    providedIn: 'root'
})
export class AuthorService {
    private url=environment.APIUrl + 'Author'
    constructor(
        private http: HttpClient
    ) { }

    public getAll(): Observable<GetAllAuthorsView> {
        return this.http.get<GetAllAuthorsView>(this.url + "/GetAll");
    }

    public addAuthor(authorView: AddAuthorViewItem){
        return this.http.post(this.url + "/Create", authorView)
    }

    public updateAuthor(authorView: UpdateAuthorViewItem){
        return this.http.post(this.url +"/Update", authorView);
    }

    public deleteAuthor(id: number){
        var temp: DeleteAuthorViewItem ={
            id: id
        }

        return this.http.post(this.url +"/Delete", temp);
    }
    
}
