import { CanActivate, Router } from "@angular/router";
import { Injectable } from "@angular/core";
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class LoginGuard implements CanActivate {

    canActivate() {
        return this.checkIfLoggedIn();
    }

    public loggedIn: boolean = false

    constructor(private cookieService: CookieService, private router: Router){}
    
    private checkIfLoggedIn(): boolean {

        // A call to the actual login service would go here
        // For now we'll just randomly return true or false

        if (this.cookieService.check("UserId")) {
            this.loggedIn= true
        }
        if (!this.loggedIn) {
            this.router.navigate([''])
        }

        return this.loggedIn;
    }
}