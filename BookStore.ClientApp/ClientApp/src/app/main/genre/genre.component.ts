import { Component, OnInit } from '@angular/core';
import { GenreService } from '../../../shared/services/genre.service';
import 'core-js/es7/reflect';
import { CookieService } from 'ngx-cookie-service';
import { MatDialog } from '@angular/material';
import { AddGenrePopup } from './popups/add-genre-popup/add-genre-popup.component';
import { DeleteGenrePopup } from './popups/delete-genre-popup/delete-genre-popup.component';
import { UpdateGenrePopup } from './popups/update-genre-popup/update-genre-popup.component';
import { GetAllGenresViewItem, GetAllGenresView } from 'src/shared/models/genre/get-all-genres.view';
import { AddGenreViewItem } from 'src/shared/models/genre/add-genre-view';
import { UpdateGenreViewItem } from 'src/shared/models/genre/update-genre.view';

export interface DialogData {
  genres: GetAllGenresViewItem[]
}

@Component({
  selector: 'genres-app',
  templateUrl: './genre.component.html',
  styleUrls: ['./genre.component.css']
})

export class GenreComponent implements OnInit {
  public genres: GetAllGenresView = new GetAllGenresView()
  public isVisible: boolean = false
  public updateGenreView: UpdateGenreViewItem = new UpdateGenreViewItem()
  public addGenreView: AddGenreViewItem = new AddGenreViewItem()
  public isAscending: boolean = true
  public searchText: string

  constructor(private genreService: GenreService, private cookie: CookieService, private dialog: MatDialog) { }

  ngOnInit() {
    this.loadGenres();
    this.checkRole();
  }

  openAddPopup(): void {
    const dialogRef = this.dialog.open(AddGenrePopup, {
      width: '330px',
      data: { }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.loadGenres()
    })
  }

  openUpdatePopup(): void {
    const dialogRef = this.dialog.open(UpdateGenrePopup, {
      width: '330px',
      data: { genres: this.genres.genres }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.loadGenres()
    })
  }

  openDeletePopup(): void {
    const dialogRef = this.dialog.open(DeleteGenrePopup, {
      width: '330px',
      data: { }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.loadGenres()
    })
  }

  public checkRole() {
    if (this.cookie.get("UserRole") == "admin") {
      this.isVisible = true;
    }
  }

  private loadGenres(){
    this.genreService.getAll().subscribe(data => { this.genres = data ;});
  }

  private sortByName(){
    if(this.isAscending){
      this.sortByNameDesc()
      this.isAscending = false
      return
    }
    this.sortByNameAsc()
    this.isAscending = true
  }

  public sortByNameDesc() {
    this.genres.genres.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
  }

  public sortByNameAsc() {
    this.genres.genres.sort((a, b) => (a.name < b.name) ? 1 : ((b.name < a.name) ? -1 : 0))
  }

  public sortByDbIdAsc() {
    this.genres.genres.sort((a, b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0))
    this.searchText = ''
  }
}