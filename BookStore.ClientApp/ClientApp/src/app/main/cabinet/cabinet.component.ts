import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { OrderService } from 'src/shared/services/order.service';
import { UserOrder } from '../entities/user-order';
import { GetAllOrdersView } from 'src/shared/models/order/get-all-orders.view';

@Component({
  selector: 'cabinet-app',
  templateUrl: './cabinet.component.html',
  styleUrls: ['./cabinet.component.css']
})

export class CabinetComponent implements OnInit {
  public userName: string = this.cookie.get("UserName")
  public userOrders: UserOrder = new UserOrder()
  public orders: GetAllOrdersView = new GetAllOrdersView()
  public open: boolean = false
  
  constructor(private cookie: CookieService, private orderService: OrderService) { }

  ngOnInit() {
    this.orderService.getUserOrder(this.cookie.get("UserId")).subscribe(data => { this.userOrders = data;})
    this.orderService.getAll().subscribe(data => {this.orders = data})
  }
}