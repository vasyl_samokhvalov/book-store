import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BookService } from 'src/shared/services/book.service';

export interface DialogData {
}

@Component({
    selector: 'delete-book-popup',
    templateUrl: 'delete-book-popup.component.html',
    styleUrls: ['./delete-book-popup.component.css']
})

export class DeleteBookPopup {
    public deleteId: number

    constructor(
        public dialogRef: MatDialogRef<DeleteBookPopup>,
        public bookService: BookService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close()
    }

    public close(){
        this.dialogRef.close()
    }
    
    public deleteBook() {
        this.bookService.deleteBook(this.deleteId).subscribe()
        this.close()
    }
}
