import { Component, OnInit } from '@angular/core';
import { PublisherService } from '../../../shared/services/publisher.service';
import { GetAllPublishersView } from '../../../shared/models/publisher/get-all-publishers.view';
import 'core-js/es7/reflect';
import { isNullOrUndefined } from 'util';
import { CookieService } from 'ngx-cookie-service';
import { AddPublisherPopup } from './popups/add-publisher-popup/add-publisher-popup.component';
import { MatDialog } from '@angular/material';
import { UpdatePublisherPopup } from './popups/update-publisher-popup/update-publisher-popup.component';
import { DeletePublisherPopup } from './popups/delete-publisher-popup/delete-publisher-popup.component';
import { AddPublisherViewItem } from 'src/shared/models/publisher/add-publisher.view';
import { UpdatePublisherViewItem } from 'src/shared/models/publisher/update-publisher.view';

@Component({
  selector: 'publishers-app',
  templateUrl: './publisher.component.html',
  styleUrls: ['./publisher.component.css']
})

export class PublisherComponent implements OnInit {
  public publishers: GetAllPublishersView = new GetAllPublishersView()
  public isVisible: boolean = false
  public updatePublisherView: UpdatePublisherViewItem = new UpdatePublisherViewItem()
  public addPublisherView: AddPublisherViewItem = new AddPublisherViewItem()
  public isAscending: boolean = true
  public searchText: string

  constructor(private publisherService: PublisherService, private cookie: CookieService, private dialog: MatDialog) { }


  ngOnInit() {
    this.loadPublishers();
    this.checkRole();
  }

  private loadPublishers(){
    this.publisherService.getAll().subscribe(data => { this.publishers = data ;});
  }

  openAddPopup(): void {
    const dialogRef = this.dialog.open(AddPublisherPopup, {
      width: '330px',
      data: { publishers: this.publishers.publishers }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.loadPublishers()
    })
  }

  openUpdatePopup(): void {
    const dialogRef = this.dialog.open(UpdatePublisherPopup, {
      width: '330px',
      data: { publishers: this.publishers.publishers }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.loadPublishers()
    })
  }

  openDeletePopup(): void {
    const dialogRef = this.dialog.open(DeletePublisherPopup, {
      width: '330px',
      data: { }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.loadPublishers()
    })
  }

  public sortByDbIdAsc() {
    this.publishers.publishers.sort((a, b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0))
    this.searchText = ''  
  }

  private sortByName(){
    if(this.isAscending){
      this.sortByNameDesc()
      this.isAscending = false
      return
    }
    this.sortByNameAsc()
    this.isAscending = true
  }

  public sortByNameDesc() {
    this.publishers.publishers.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
  }

  public sortByNameAsc() {
    this.publishers.publishers.sort((a, b) => (a.name < b.name) ? 1 : ((b.name < a.name) ? -1 : 0))
  }

  public checkRole() {
    if (this.cookie.get("UserRole") == "admin") {
      this.isVisible = true;
    }
  }

  public getPublisher() {
    if (!isNullOrUndefined(this.updatePublisherView.id)) {
      this.updatePublisherView.name = this.publishers.publishers[this.updatePublisherView.id].name
    }
  }
}