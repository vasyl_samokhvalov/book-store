import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PublisherService } from 'src/shared/services/publisher.service';

export interface DialogData {
}

@Component({
    selector: 'delete-publisher-popup',
    templateUrl: 'delete-publisher-popup.component.html',
    styleUrls: ['./delete-publisher-popup.component.css']
})

export class DeletePublisherPopup {
    public deleteId: number

    constructor(
        public dialogRef: MatDialogRef<DeletePublisherPopup>,
        public publisherService: PublisherService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close()
    }

    public close() {
        this.dialogRef.close()
    }

    public deletePublisher() {
        this.publisherService.deletePublisher(this.deleteId).subscribe()
        this.close()
    }
}
