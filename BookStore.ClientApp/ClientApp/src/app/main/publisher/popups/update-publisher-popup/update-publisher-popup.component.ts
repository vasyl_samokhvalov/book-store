import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { isNullOrUndefined } from 'util';
import { PublisherService } from 'src/shared/services/publisher.service';
import { UpdatePublisherViewItem } from 'src/shared/models/publisher/update-publisher.view';
import { GetAllPublishersViewItem } from 'src/shared/models/publisher/get-all-publishers.view';

export interface DialogData {
  publishers: GetAllPublishersViewItem[]
}

@Component({
  selector: 'update-publisher-popup',
  templateUrl: 'update-publisher-popup.component.html',
  styleUrls: ['./update-publisher-popup.component.css']
})

export class UpdatePublisherPopup {
  public updatePublisherView: UpdatePublisherViewItem = new UpdatePublisherViewItem()

  constructor(
    public dialogRef: MatDialogRef<UpdatePublisherPopup>,
    public publisherService: PublisherService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close()
  }

  public close() {
    this.dialogRef.close()
  }

  public getPublisher() {
    if (!isNullOrUndefined(this.updatePublisherView.id)) {
      this.updatePublisherView.name = this.data.publishers[this.updatePublisherView.id].name
    }
  }

  public updatePublisher() {
    var temp: UpdatePublisherViewItem = new UpdatePublisherViewItem()
    temp = this.data.publishers[this.updatePublisherView.id]
    if (!isNullOrUndefined(this.updatePublisherView.name)) {
      temp.name = this.updatePublisherView.name
    }
    this.publisherService.updatePublisher(temp).subscribe()
    this.close()
  }
}
