import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PublisherService } from 'src/shared/services/publisher.service';
import { AddPublisherViewItem } from 'src/shared/models/publisher/add-publisher.view';

export interface DialogData {
}

@Component({
    selector: 'add-publisher-popup',
    templateUrl: 'add-publisher-popup.component.html',
    styleUrls: ['./add-publisher-popup.component.css']
})

export class AddPublisherPopup {
    public addPublisherView: AddPublisherViewItem = new AddPublisherViewItem()

    constructor(
        public dialogRef: MatDialogRef<AddPublisherPopup>,
        private publisherService: PublisherService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close()
    }

    public close() {
        this.dialogRef.close()
    }

    public addPublisher() {
        this.publisherService.addPublisher(this.addPublisherView).subscribe()        
        this.close()
    }
}
