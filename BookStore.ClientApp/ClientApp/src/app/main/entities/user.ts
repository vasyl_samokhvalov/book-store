export class User{
    public id: number
    public name : string
    public surname : string
    public email : string
    public phone : string
    public login : string
    public password : string
}