import { Component, OnInit } from '@angular/core';
import { BookAuthorService } from '../../../shared/services/book-author.service';
import 'core-js/es7/reflect';
import { GetAllBookAuthorsView } from 'src/shared/models/book-author/get-all-book-authors.view';
import { AddBookAuthorViewItem } from 'src/shared/models/book-author/add-book-author.view';
import { UpdateBookAuthorViewItem } from 'src/shared/models/book-author/update-book-author.view';

@Component({
  selector: 'bookauthors-app',
  templateUrl: './bookauthor.component.html'
})

export class BookAuthorComponent implements OnInit {
  public data: GetAllBookAuthorsView = new GetAllBookAuthorsView();

  constructor(private bookauthorService: BookAuthorService) { }


  ngOnInit() {
    this.loadBookAuthors();
  }

  private loadBookAuthors(){
    this.bookauthorService.getAll().subscribe(data => { this.data = data ;});
  }


  public addBookAuthor(bookId: number, authorId: number){
    var temp: AddBookAuthorViewItem = new AddBookAuthorViewItem()
    this.bookauthorService.addBookAuthor(temp).subscribe()
  }

  public deleteBookAuthor(id: number){
    this.bookauthorService.deleteBookAuthor(id).subscribe()
  }
  
  private updateBookAuthor(id: number, name: string, cost:number, year:number, publisher: number){
    var temp: UpdateBookAuthorViewItem =  new UpdateBookAuthorViewItem()
    temp.id = id
    this.bookauthorService.updateBookAuthor(temp).subscribe()
  }
}