import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { OrderService } from 'src/shared/services/order.service';

export interface DialogData {
}

@Component({
    selector: 'delete-order-popup',
    templateUrl: 'delete-order-popup.component.html',
    styleUrls: ['./delete-order-popup.component.css']
})

export class DeleteOrderPopup {
    public deleteId: number

    constructor(
        public dialogRef: MatDialogRef<DeleteOrderPopup>,
        public orderService: OrderService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close()
    }

    public close(){
        this.dialogRef.close()
    }
    
    public deleteOrder() {
        this.orderService.deleteOrder(this.deleteId).subscribe()
        this.close()
    }
}
