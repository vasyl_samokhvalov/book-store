import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { JournalService } from 'src/shared/services/journal.service';

export interface DialogData {
}

@Component({
    selector: 'delete-journal-popup',
    templateUrl: 'delete-journal-popup.component.html',
    styleUrls: ['./delete-journal-popup.component.css']
})

export class DeleteJournalPopup {
    public deleteId: number
    public publisher
    public author

    constructor(
        public dialogRef: MatDialogRef<DeleteJournalPopup>,
        public journalService: JournalService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close()
    }

    public close(){
        this.dialogRef.close()
    }
    
    public deleteJournal() {
        this.journalService.deleteJournal(this.deleteId).subscribe()
        this.close()
    }
}
