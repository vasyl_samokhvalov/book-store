import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { LoginComponent } from '../../login/login.component';
import { MatDialog } from '@angular/material/dialog';
import { BasketPopup } from '../basket-popup/basket-popup.component';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.css']
})
export class NavigationBarComponent {
  public isVisible: boolean = false;
  public head: string
  public isLogged: boolean = false

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver, private cookie: CookieService, private login: LoginComponent, public dialog: MatDialog) { }

  ngOnInit() {
    this.checkRole();
    this.checkLogin();
    if (isNullOrUndefined(localStorage.getItem("BookBasket"))) { localStorage.setItem("BookBasket", '[]') }
    if (isNullOrUndefined(localStorage.getItem("JournalBasket"))) { localStorage.setItem("JournalBasket", '[]') }
  }

  openBasket(): void {
    const dialogRef = this.dialog.open(BasketPopup, {
      width: '500px',
      data: { }
    })

    dialogRef.afterClosed().subscribe(result => {
    })
  }

  public checkRole() {
    if (this.cookie.get("UserRole") == "admin") {
      this.isVisible = true;
    }
  }

  public checkLogin() {
    if (this.cookie.check("UserId")) {
      this.isLogged = true
      return
    }
    this.isLogged = false
  }

  public logout() {
    this.login.logout();
  }
}
