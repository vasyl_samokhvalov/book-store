import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthorService } from 'src/shared/services/author.service';

export interface DialogData {
}

@Component({
    selector: 'delete-author-popup',
    templateUrl: 'delete-author-popup.component.html',
    styleUrls: ['./delete-author-popup.component.css']
})

export class DeleteAuthorPopup {
    public deleteId: number

    constructor(
        public dialogRef: MatDialogRef<DeleteAuthorPopup>,
        public authorService: AuthorService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close()
    }

    public close() {
        this.dialogRef.close()
    }

    public deleteAuthor() {
        this.authorService.deleteAuthor(this.deleteId).subscribe()
        this.close()
    }
}
