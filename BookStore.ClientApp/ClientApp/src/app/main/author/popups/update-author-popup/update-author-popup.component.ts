import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { isNullOrUndefined } from 'util';
import { AuthorService } from 'src/shared/services/author.service';
import { GetAllAuthorsViewItem } from 'src/shared/models/author/get-all-authors.view.';
import { UpdateAuthorViewItem } from 'src/shared/models/author/update-author.view';

export interface DialogData {
  authors: GetAllAuthorsViewItem[]
}

@Component({
  selector: 'update-author-popup',
  templateUrl: 'update-author-popup.component.html',
  styleUrls: ['./update-author-popup.component.css']
})

export class UpdateAuthorPopup {
  public updateAuthorView: UpdateAuthorViewItem = new UpdateAuthorViewItem()

  constructor(
    public dialogRef: MatDialogRef<UpdateAuthorPopup>,
    public authorService: AuthorService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close()
  }

  public close() {
    this.dialogRef.close()
  }

  public getAuthor() {
    if (!isNullOrUndefined(this.updateAuthorView.id)) {
      this.updateAuthorView.name = this.data.authors[this.updateAuthorView.id].name
      this.updateAuthorView.surname = this.data.authors[this.updateAuthorView.id].surname
    }
  }

  public updateAuthor() {
    var temp: UpdateAuthorViewItem = new UpdateAuthorViewItem()
    temp = this.data.authors[this.updateAuthorView.id]
    if (!isNullOrUndefined(this.updateAuthorView.name)) {
      temp.name = this.updateAuthorView.name
    }
    if (!isNullOrUndefined(this.updateAuthorView.surname)) {
      temp.surname = this.updateAuthorView.surname
    }
    this.authorService.updateAuthor(temp).subscribe()
    this.close()
  }
}
