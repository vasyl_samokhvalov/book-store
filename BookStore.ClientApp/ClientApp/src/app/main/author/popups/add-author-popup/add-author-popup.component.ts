import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthorService } from 'src/shared/services/author.service';
import { AddAuthorViewItem } from 'src/shared/models/author/add-author.view';
import { UpdateAuthorViewItem } from 'src/shared/models/author/update-author.view';
import { GetAllAuthorsViewItem } from 'src/shared/models/author/get-all-authors.view.';

export interface DialogData {
    authors: GetAllAuthorsViewItem[]
}

@Component({
    selector: 'add-author-popup',
    templateUrl: 'add-author-popup.component.html',
    styleUrls: ['./add-author-popup.component.css']
})

export class AddAuthorPopup {
    public addAuthorView: AddAuthorViewItem = new AddAuthorViewItem()
    public updateAuthorView: UpdateAuthorViewItem = new UpdateAuthorViewItem();

    constructor(
        public dialogRef: MatDialogRef<AddAuthorPopup>,
        private authorService: AuthorService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close()
    }

    public close() {
        this.dialogRef.close()
    }

    public addAuthor() {
        this.authorService.addAuthor(this.addAuthorView).subscribe()
        this.close()
    }
}
