import { Component, OnInit } from '@angular/core';
import 'core-js/es7/reflect';
import { CookieService } from 'ngx-cookie-service';
import { AuthorService } from '../../../shared/services/author.service';
import { MatDialog } from '@angular/material';
import { AddAuthorPopup } from './popups/add-author-popup/add-author-popup.component';
import { DeleteAuthorPopup } from './popups/delete-author-popup/delete-author-popup.component';
import { UpdateAuthorPopup } from './popups/update-author-popup/update-author-popup.component';
import { GetAllAuthorsViewItem, GetAllAuthorsView } from '../../../shared/models/author/get-all-authors.view.';
import { UpdateAuthorViewItem } from 'src/shared/models/author/update-author.view';
import { AddAuthorViewItem } from 'src/shared/models/author/add-author.view';

export interface DialogData {
  authors: GetAllAuthorsViewItem[]
}

@Component({
  selector: 'authors-app',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css']
})

export class AuthorComponent implements OnInit {
  public authors: GetAllAuthorsView = new GetAllAuthorsView();
  public isVisible: boolean = false;
  public updateAuthorView: UpdateAuthorViewItem = new UpdateAuthorViewItem();
  public addAuthorView: AddAuthorViewItem = new AddAuthorViewItem();
  public isAscending: boolean = true;
  public searchText: string

  constructor(private authorService: AuthorService, private cookie: CookieService, public dialog: MatDialog) { }


  ngOnInit() {
    this.loadAuthors();
    this.checkRole();
  }

  openAddPopup(): void {
    const dialogRef = this.dialog.open(AddAuthorPopup, {
      width: '330px',
      data: { authors: this.authors.authors }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.loadAuthors()
    })
  }

  openUpdatePopup(): void {
    const dialogRef = this.dialog.open(UpdateAuthorPopup, {
      width: '330px',
      data: { authors: this.authors.authors }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.loadAuthors()
    })
  }

  openDeletePopup(): void {
    const dialogRef = this.dialog.open(DeleteAuthorPopup, {
      width: '330px',
      data: { }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.loadAuthors()
    })
  }

  private loadAuthors() {
    this.authorService.getAll().subscribe(data => { this.authors = data; });
  }

  public sortByDbIdAsc() {
    this.authors.authors.sort((a, b) => (a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0))
    this.searchText = ''
  }

  private sortByName() {
    if (this.isAscending) {
      this.sortByNameDesc()
      this.isAscending = false
      return
    }
    this.sortByNameAsc()
    this.isAscending = true
  }

  public sortByNameDesc() {
    this.authors.authors.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
    //this.data.authors.sort((a,b) => (a[prop] > b[prop]) ? 1 : ((b[prop] > a[prop]) ? -1 : 0))
  }

  public sortByNameAsc() {
    this.authors.authors.sort((a, b) => (a.name < b.name) ? 1 : ((b.name < a.name) ? -1 : 0))
    //this.data.authors.sort((a,b) => (a[prop] > b[prop]) ? 1 : ((b[prop] > a[prop]) ? -1 : 0))
  }

  private sortBySurname() {
    if (this.isAscending) {
      this.sortBySurnameDesc()
      this.isAscending = false
      return
    }
    this.sortBySurnameAsc()
    this.isAscending = true
  }

  public sortBySurnameDesc() {
    this.authors.authors.sort((a, b) => (a.surname > b.surname) ? 1 : ((b.surname > a.surname) ? -1 : 0))
    //this.data.authors.sort((a,b) => (a[prop] > b[prop]) ? 1 : ((b[prop] > a[prop]) ? -1 : 0))
  }

  public sortBySurnameAsc() {
    this.authors.authors.sort((a, b) => (a.surname < b.surname) ? 1 : ((b.surname < a.surname) ? -1 : 0))
    //this.data.authors.sort((a,b) => (a[prop] > b[prop]) ? 1 : ((b[prop] > a[prop]) ? -1 : 0))
  }

  public checkRole() {
    if (this.cookie.get("UserRole") == "admin") {
      this.isVisible = true;
    }
  }
}
