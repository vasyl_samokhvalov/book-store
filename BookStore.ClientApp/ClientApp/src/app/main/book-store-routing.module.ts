import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthorComponent } from './author/author.component';
import { LoginGuard } from '../login-guard.component';
import { BookComponent } from './book/book.component';
import { GenreComponent } from './genre/genre.component';
import { JournalComponent } from './journal/journal.component';
import { OrderComponent } from './order/order.component';
import { PublisherComponent } from './publisher/publisher.component';
import { UserComponent } from './user/user.component';
import { LoginComponent } from '../login/login.component';
import { RegisterComponent } from '../register/register.component';
import { CabinetComponent } from './cabinet/cabinet.component';
import { NotFoundComponent } from '../not-found/not-found.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'authors', component: AuthorComponent, canActivate: [LoginGuard] },
  {
    path: 'books', component: BookComponent, canActivate: [LoginGuard],
    children: [
      { path: ':publisher', component: BookComponent },
      { path: ':author', component: BookComponent }
    ]
  },
  { path: 'genres', component: GenreComponent, canActivate: [LoginGuard] },
  { path: 'journals', component: JournalComponent, canActivate: [LoginGuard] },
  { path: 'orders', component: OrderComponent, canActivate: [LoginGuard] },
  { path: 'publishers', component: PublisherComponent, canActivate: [LoginGuard] },
  { path: 'users', component: UserComponent, canActivate: [LoginGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'cabinet', component: CabinetComponent, canActivate: [LoginGuard] },

  //last
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class BookStoreRoutingModule { }
