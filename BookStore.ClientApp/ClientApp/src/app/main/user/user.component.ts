import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../shared/services/user.service';
import { GetAllUsersView } from '../../../shared/models/user/get-all-users.view';
import 'core-js/es7/reflect';
import { RegisterViewItem } from 'src/shared/models/user/register-user.view';
import { UpdateUserViewItem } from 'src/shared/models/user/update-user.view';

@Component({
  selector: 'users-app',
  templateUrl: '/user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent implements OnInit {
  public data: GetAllUsersView = new GetAllUsersView();

  constructor(private userService: UserService) { }


  ngOnInit() {
    this.loadUsers();
  }
  public getAll(){
    this.userService.getAll().subscribe(data => { this.data = data ;});
  }
  
  private loadUsers(){
    this.userService.getAll().subscribe(data => { this.data = data ;});
  }


  public addUser(name: string){
    var temp: RegisterViewItem = new RegisterViewItem()
    temp.email = name
    this.userService.addUser(temp).subscribe()
  }

  public deleteUser(id: number){
    this.userService.deleteUser(id).subscribe()
  }
  
  private updateUser(id: number, name: string, cost:number, year:number, publisher: number){
    var temp: UpdateUserViewItem =  new UpdateUserViewItem()
    temp.id = id
    temp.name = name
    this.userService.updateUser(temp).subscribe()
  }
}