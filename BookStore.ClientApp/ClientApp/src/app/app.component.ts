import * as core from '@angular/core';
import 'core-js/es7/reflect';

@core.Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})

export class AppComponent implements core.OnInit {
  ngOnInit() {
  }
}