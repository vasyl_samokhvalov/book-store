import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../shared/services/login.service';
import { isNullOrUndefined } from 'util';
import { RegisterViewItem } from 'src/shared/models/user/register-user.view';


@Component({
  selector: 'register-app',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {
  public passwordReg: string;
  public emailReg: string;
  public confirmPassword: string;
  public isMatching: boolean = true;

  constructor(private loginService: LoginService) { }

  ngOnInit() {
  }

  public register(){
    if(isNullOrUndefined(this.confirmPassword)){
      return
    }
    var user: RegisterViewItem =  new RegisterViewItem()
    user.name = name
    user.email = this.emailReg
    user.password = this.passwordReg
    this.loginService.register(user).subscribe()
  }

  public checkPassMatch(){
    if(this.passwordReg != this.confirmPassword){
      this.isMatching = false
      return
    }
    this.isMatching = true
  }
}