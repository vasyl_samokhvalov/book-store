import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { AppComponent } from './app.component'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { LoginComponent } from './login/login.component'
import { CookieService } from 'ngx-cookie-service'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NavigationBarComponent } from './main/navigation-bar/navigation-bar.component'
import { LayoutModule } from '@angular/cdk/layout'
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatDialogModule, MatFormFieldModule, MatSelectModule, MatTooltipModule } from '@angular/material'
import { LoginGuard } from './login-guard.component'
import { ErrorInterceptor } from 'src/shared/modules/error-handler.module'
import { Ng2SearchPipeModule } from 'ng2-search-filter'
import { BookStoreRoutingModule } from './main/book-store-routing.module';
import { BookStoreModule } from './main/book-store.module';
import { BookStoreComponent } from './main/book-store.component';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
    imports: [BrowserModule,
        FormsModule,
        HttpClientModule,
        BookStoreRoutingModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        LayoutModule,
        MatToolbarModule,
        MatFormFieldModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule,
        MatDialogModule,
        MatSelectModule,
        Ng2SearchPipeModule,
        MatTooltipModule,
        BookStoreModule,
        ToastrModule.forRoot()],
    declarations: [AppComponent,
        LoginComponent,
        BookStoreComponent, NavigationBarComponent],
    bootstrap: [AppComponent, BookStoreComponent],
    entryComponents: [
        BookStoreComponent],
    providers: [LoginComponent,
        CookieService,
        LoginGuard,
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }]
})
export class AppModule {
}