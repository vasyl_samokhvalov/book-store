﻿using BookStore.DataAccess.Entities;
using System.Collections.Generic;


namespace BookStore.DataAccess.RepositoryInterfaces
{
    public interface IBookAuthorRepository : IBaseRepository<BookAuthor>
    {
        long Create(BookAuthor bookAuthor);
        void Delete(int id);
        BookAuthor Get(int id);
        List<BookAuthor> GetAll(int id);
        List<BookAuthor> GetAll();
        void Update(BookAuthor bookAuthor);
    }
}