﻿using BookStore.DataAccess.Entities;
using System.Collections.Generic;

namespace BookStore.DataAccess.RepositoryInterfaces
{
    public interface IBookGenreRepository : IBaseRepository<BookGenre>
    {
        long Create(BookGenre bookGenre);
        void Delete(int id);
        BookGenre Get(int id);
        List<BookGenre> GetAll();
        void Update(BookGenre bookGenre);
    }
}