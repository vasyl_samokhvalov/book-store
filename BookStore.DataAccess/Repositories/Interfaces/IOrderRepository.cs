﻿using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Entities.AddOns;
using System.Collections.Generic;

namespace BookStore.DataAccess.RepositoryInterfaces
{
    public interface IOrderRepository : IBaseRepository<Order>
    {
        long Create(Order order);
        void Update(Order order);
        void Delete(int id);
        Order Get(int id);
        List<Order> GetAll();
        List<UserOrder> GetUserOrder(string userId);
        List<GetAllBookOrderFullViewModel> GetAllBig();
    }
}