﻿using BookStore.DataAccess.Entities;
using System.Collections.Generic;

namespace BookStore.DataAccess.RepositoryInterfaces
{
    public interface IJournalOrderRepository : IBaseRepository<JournalOrder>
    {
        long Create(JournalOrder journalOrder);
        void Delete(int id);
        JournalOrder Get(int id);
        List<JournalOrder> GetAll();
        void Update(JournalOrder journalOrder);
    }
}