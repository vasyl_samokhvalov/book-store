﻿namespace BookStore.DataAccess.RepositoryInterfaces
{
    public interface IBaseRepository<T>
    {
        long Create(T entity);
        void Update(T entity);
        void Delete(int id);
        T Get(int id);
    }
}
