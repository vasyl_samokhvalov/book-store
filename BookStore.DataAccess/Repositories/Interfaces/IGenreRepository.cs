﻿using BookStore.DataAccess.Entities;
using System.Collections.Generic;

namespace BookStore.DataAccess.RepositoryInterfaces
{
    public interface IGenreRepository : IBaseRepository<Genre>
    {
        long Create(Genre genre);
        void Update(Genre genre);
        void Delete(int id);
        Genre Get(int id);
        List<Genre> GetAll();
    }
}