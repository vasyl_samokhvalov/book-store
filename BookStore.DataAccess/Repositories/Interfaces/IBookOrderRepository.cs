﻿using BookStore.DataAccess.Entities;
using System.Collections.Generic;

namespace BookStore.DataAccess.RepositoryInterfaces
{
    public interface IBookOrderRepository : IBaseRepository<BookOrder>
    {
        long Create(BookOrder bookOrder);
        void Delete(int id);
        BookOrder Get(int id);
        List<BookOrder> GetAll();
        void Update(BookOrder bookOrder);
    }
}