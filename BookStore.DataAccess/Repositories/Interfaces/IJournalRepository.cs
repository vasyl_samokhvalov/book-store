﻿using BookStore.DataAccess.Entities;
using System.Collections.Generic;


namespace BookStore.DataAccess.RepositoryInterfaces
{
    public interface IJournalRepository : IBaseRepository<Journal>
    {
        long Create(Journal journal);
        void Update(Journal journal);
        void Delete(int id);
        Journal Get(int id);
        List<Journal> GetAll();
    }
}