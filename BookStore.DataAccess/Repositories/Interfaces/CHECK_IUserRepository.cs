﻿using BookStore.DataAccess.Entities;
using System.Collections.Generic;


namespace BookStore.DataAccess.RepositoryInterfaces
{
    public interface IUserRepository : IBaseRepository<User>
    {
        long Create(User user);
        void Update(User user);
        void Delete(int id);
        User Get(int id);
        List<User> GetAll();
    }
}