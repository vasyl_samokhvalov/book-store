﻿using BookStore.DataAccess.Entities;
using System.Collections.Generic;

namespace BookStore.DataAccess.RepositoryInterfaces
{
    public interface IBookJournalRepository : IBaseRepository<BookJournal>
    {
        long Create(BookJournal bookJournal);
        void Delete(int id);
        BookJournal Get(int id);
        List<BookJournal> GetAll();
        void Update(BookJournal bookJournal);
    }
}