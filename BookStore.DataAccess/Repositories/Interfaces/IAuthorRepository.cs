﻿using BookStore.DataAccess.Entities;
using System.Collections.Generic;


namespace BookStore.DataAccess.RepositoryInterfaces
{
    public interface IAuthorRepository : IBaseRepository<Author>
    {
        long Create(Author author);
        void Update(Author author);
        void Delete(int id);
        Author Get(int id);
        List<Author> GetAll();
    }
}