﻿using BookStore.DataAccess.Entities;
using System.Collections.Generic;


namespace BookStore.DataAccess.RepositoryInterfaces
{
    public interface IPublisherRepository : IBaseRepository<Publisher>
    {
        long Create(Publisher publisher);
        void Update(Publisher publisher);
        void Delete(int id);
        Publisher Get(int id);
        List<Publisher> GetAll();
    }
}