﻿using BookStore.DataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DataAccess.RepositoryInterfaces
{
    public interface IBookRepository : IBaseRepository<Book>
    {
        Task<int> Create(Book book);
        void Update(Book book);
        void Delete(int id);
        Task<Book> Get(int id);
        Task<List<Book>> GetAll();
        List<Book> GetBasket(List<int> id);
    }
}