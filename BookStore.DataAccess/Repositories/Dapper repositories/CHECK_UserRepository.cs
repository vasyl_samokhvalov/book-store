﻿//using BookStore.DataAccess.Entities;
//using Dapper;
//using System.Collections.Generic;
//using System.Data;
//using System.Data.SqlClient;
//using System.Linq;
//using BookStore.DataAccess.RepositoryInterfaces;

//namespace BookStore.DataAccess.Repositories
//{
//    public class UserRepository : IUserRepository
//    {
//        public IDbConnection dbConnection = new SqlConnection("Server=DESKTOP-VVGE7A7;Database=BookStoreDB3;Trusted_Connection=True;");

//        public List<User> GetAll()
//        {
//            using (dbConnection)
//            {
//                return dbConnection.Query<User>("SELECT * FROM AspNetUsers").ToList();
//            }
//        }

//        public User Get(int id)
//        {
//            using (dbConnection)
//            {
//                return dbConnection.Query<User>("SELECT * FROM AspNetUsers WHERE Id = @id", new { id }).FirstOrDefault();
//            }
//        }

//        public long Create(User user)
//        {
//            using (dbConnection)
//            {
//                var sqlQuery = "INSERT INTO AspNetUsers (Name, Surname, Email, Phone, Login, Password) VALUES(@Name, @Surname, @Email, @Phone, @Login, @Password); SELECT CAST(SCOPE_IDENTITY() as int)";
//                int? userId = dbConnection.Query<int>(sqlQuery, user).FirstOrDefault();
//                return userId.Value;
//            }
//        }

//        public void Update(User user)
//        {
//            using (dbConnection)
//            {
//                var sqlQuery = "UPDATE User SET Name = @Name, Surname = @Surname, Email = @Email, Phone = @Phone, Login = @Login, Password = @Password WHERE Id = @Id";
//                dbConnection.Execute(sqlQuery, user);
//            }
//        }

//        public void Delete(int id)
//        {
//            using (dbConnection)
//            {
//                var sqlQuery = "DELETE FROM User WHERE Id = @id";
//                dbConnection.Execute(sqlQuery, new { id });
//            }
//        }
//    }
//}