﻿using BookStore.DataAccess.Entities;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using BookStore.DataAccess.RepositoryInterfaces;


namespace BookStore.DataAccess.DapperRepositories
{
    public class BookGenreRepository : BaseRepository<BookGenre>, IBookGenreRepository
    {
        public BookGenreRepository(string conn)
        {
            connectionString = conn;
        }

        public List<BookGenre> GetAll()
        {
            using (dbConnection)
            {
                return dbConnection.Query<BookGenre>("SELECT * FROM BookGenre").ToList();
            }
        }

        public BookGenre Get(int id)
        {
            using (dbConnection)
            {
                return dbConnection.Query<BookGenre>("SELECT * FROM BookGenre WHERE Id = @id", new { id }).FirstOrDefault();
            }
        }

        public long Create(BookGenre bookGenre)
        {
            using (dbConnection)
            {
                bookGenre.CreationDate = DateTime.Now;

                var sqlQuery = "INSERT INTO BookGenre (CreationDate, BookId, GenreId) VALUES(@CreationDate, @BookId, @GenreId); SELECT CAST(SCOPE_IDENTITY() as int)";
                int? bookGenreId = dbConnection.Query<int>(sqlQuery, bookGenre).FirstOrDefault();
                return bookGenreId.Value;
            }
        }

        public void Update(BookGenre bookGenre)
        {
            using (dbConnection)
            {
                bookGenre.CreationDate = DateTime.Now;

                var sqlQuery = "UPDATE BookGenre SET CreationDate=@CreationDate, BookId=@BookId, GenreId=@GenreId WHERE Id = @Id";
                dbConnection.Execute(sqlQuery, bookGenre);
            }
        }

        public void Delete(int id)
        {
            using (dbConnection)
            {
                var sqlQuery = "DELETE FROM BookGenre WHERE Id = @id";
                dbConnection.Execute(sqlQuery, new { id });
            }
        }
    }
}