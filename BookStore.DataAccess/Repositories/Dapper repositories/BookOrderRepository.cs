﻿using BookStore.DataAccess.Entities;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using BookStore.DataAccess.RepositoryInterfaces;


namespace BookStore.DataAccess.DapperRepositories
{
    public class BookOrderRepository : BaseRepository<BookOrder>, IBookOrderRepository
    {
        public BookOrderRepository(string conn)
        {
            connectionString = conn;
        }

        public List<BookOrder> GetAll()
        {
            using (dbConnection)
            {
                return dbConnection.Query<BookOrder>("SELECT * FROM BookOrder").ToList();
            }
        }

        public BookOrder Get(int id)
        {
            using (dbConnection)
            {
                return dbConnection.Query<BookOrder>("SELECT * FROM BookOrder WHERE Id = @id", new { id }).FirstOrDefault();
            }
        }

        public long Create(BookOrder bookOrder)
        {
            using (dbConnection)
            {
                bookOrder.CreationDate = DateTime.Now;

                var sqlQuery = "INSERT INTO BookOrder (CreationDate, BookId, OrderId) VALUES(@CreationDate, @BookId, @OrderId); SELECT CAST(SCOPE_IDENTITY() as int)";
                int? bookOrderId = dbConnection.Query<int>(sqlQuery, bookOrder).FirstOrDefault();
                return bookOrderId.Value;
            }
        }

        public void Update(BookOrder bookOrder)
        {
            using (dbConnection)
            {
                bookOrder.CreationDate = DateTime.Now;

                var sqlQuery = "UPDATE BookOrder SET CreationDate=@CreationDate, BookId=@BookId, OrderId=@OrderId WHERE Id = @Id";
                dbConnection.Execute(sqlQuery, bookOrder);
            }
        }

        public void Delete(int id)
        {
            using (dbConnection)
            {
                var sqlQuery = "DELETE FROM BookOrder WHERE Id = @id";
                dbConnection.Execute(sqlQuery, new { id });
            }
        }
    }
}