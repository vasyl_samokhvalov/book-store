﻿using BookStore.DataAccess.Entities;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using BookStore.DataAccess.RepositoryInterfaces;


namespace BookStore.DataAccess.DapperRepositories
{
    public class BookJournalRepository : BaseRepository<BookJournal>, IBookJournalRepository
    {
        public BookJournalRepository(string conn)
        {
            connectionString = conn;
        }

        public List<BookJournal> GetAll()
        {
            using (dbConnection)
            {
                return dbConnection.Query<BookJournal>("SELECT * FROM BookJournal").ToList();
            }
        }

        public BookJournal Get(int id)
        {
            using (dbConnection)
            {
                return dbConnection.Query<BookJournal>("SELECT * FROM BookJournal WHERE Id = @id", new { id }).FirstOrDefault();
            }
        }

        public long Create(BookJournal bookJournal)
        {
            using (dbConnection)
            {
                bookJournal.CreationDate = DateTime.Now;

                var sqlQuery = "INSERT INTO BookAuthor (CreationDate, BookId, JournalId) VALUES(@CreationDate, @BookId, @JournalId); SELECT CAST(SCOPE_IDENTITY() as int)";
                int? bookJournalId = dbConnection.Query<int>(sqlQuery, bookJournal).FirstOrDefault();
                return bookJournalId.Value;
            }
        }

        public void Update(BookJournal bookJournal)
        {
            using (dbConnection)
            {
                bookJournal.CreationDate = DateTime.Now;

                var sqlQuery = "UPDATE BookJournal SET CreationDate=@CreationDate, BookId=@BookId, JournalId=@JournalId WHERE Id = @Id";
                dbConnection.Execute(sqlQuery, bookJournal);
            }
        }

        public void Delete(int id)
        {
            using (dbConnection)
            {
                var sqlQuery = "DELETE FROM BookJournal WHERE Id = @id";
                dbConnection.Execute(sqlQuery, new { id });
            }
        }
    }
}