﻿using BookStore.DataAccess.Entities;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using BookStore.DataAccess.RepositoryInterfaces;


namespace BookStore.DataAccess.DapperRepositories
{
    public class GenreRepository : BaseRepository<Genre>, IGenreRepository
    {
        public GenreRepository(string conn)
        {
            connectionString = conn;
        }
        public List<Genre> GetAll()
        {
            using (dbConnection)
            {
                return dbConnection.Query<Genre>("SELECT * FROM Genres").ToList();
            }
        }

        public Genre Get(int id)
        {
            using (dbConnection)
            {
                return dbConnection.Query<Genre>("SELECT * FROM Genres WHERE Id = @id", new { id }).FirstOrDefault();
            }
        }

        public long Create(Genre genre)
        {
            using (dbConnection)
            {
                genre.CreationDate = DateTime.Now;

                var sqlQuery = "INSERT INTO Genres (CreationDate, Name) VALUES(@CreationDate, @Name); SELECT CAST(SCOPE_IDENTITY() as int)";
                int? genreId = dbConnection.Query<int>(sqlQuery, genre).FirstOrDefault();
                return genreId.Value;
            }
        }

        public void Update(Genre genre)
        {
            using (dbConnection)
            {
                genre.CreationDate = DateTime.Now;

                var sqlQuery = "UPDATE Genres SET CreationDate=@CreationDate, Name = @Name WHERE Id = @Id";
                dbConnection.Execute(sqlQuery, genre);
            }
        }

        public void Delete(int id)
        {
            using (dbConnection)
            {
                var sqlQuery = "DELETE FROM Genres WHERE Id = @id";
                dbConnection.Execute(sqlQuery, new { id });
            }
        }
    }
}