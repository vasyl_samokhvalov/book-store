﻿using BookStore.DataAccess.Entities;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using BookStore.DataAccess.RepositoryInterfaces;


namespace BookStore.DataAccess.DapperRepositories
{
    public class JournalRepository : BaseRepository<Journal>, IJournalRepository
    {
        public JournalRepository(string conn)
        {
            connectionString = conn;
        }

        public List<Journal> GetAll()
        {
            using (dbConnection)
            {
                return dbConnection.Query<Journal>("SELECT * FROM Journals").ToList();
            }
        }

        public Journal Get(int id)
        {
            using (dbConnection)
            {
                return dbConnection.Query<Journal>("SELECT * FROM Journals WHERE Id = @id", new { id }).FirstOrDefault();
            }
        }

        public long Create(Journal journal)
        {
            using (dbConnection)
            {
                journal.CreationDate = DateTime.Now;

                var sqlQuery = "INSERT INTO Journals (CreationDate, Name, Cost) VALUES(@CreationDate, @Name, @Cost); SELECT CAST(SCOPE_IDENTITY() as int)";
                int? journalId = dbConnection.Query<int>(sqlQuery, journal).FirstOrDefault();
                return journalId.Value;
            }
        }

        public void Update(Journal journal)
        {
            using (dbConnection)
            {
                journal.CreationDate = DateTime.Now;

                var sqlQuery = "UPDATE Journals SET CreationDate=@CreationDate, Name=@Name, Cost = @Cost WHERE Id = @Id";
                dbConnection.Execute(sqlQuery, journal);
            }
        }

        public void Delete(int id)
        {
            using (dbConnection)
            {
                var sqlQuery = "DELETE FROM Journals WHERE Id = @id";
                dbConnection.Execute(sqlQuery, new { id });
            }
        }
    }
}
