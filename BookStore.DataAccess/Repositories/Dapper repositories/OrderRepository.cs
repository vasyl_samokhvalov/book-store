﻿using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Entities.AddOns;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using BookStore.DataAccess.RepositoryInterfaces;


namespace BookStore.DataAccess.DapperRepositories
{
    public class OrderRepository : BaseRepository<Order>, IOrderRepository
    {
        public OrderRepository(string conn)
        {
            connectionString = conn;
        }

        public List<Order> GetAll()
        {
            using (dbConnection)
            {
                return dbConnection.Query<Order>("SELECT * FROM [Orders]").ToList();
            }
        }
        public List<UserOrder> GetUserOrder(string userId)
        {
            using (dbConnection)
            {
                return dbConnection.Query<UserOrder>("SELECT Orders.Id, Orders.CreationDate, Orders.Cost, Orders.UserId, Books.Name FROM ((([BookStoreDB].[dbo].[BookOrder] INNER JOIN Orders on Orders.Id = BookOrder.OrderId) INNER JOIN Books on Books.Id = BookOrder.BookId) INNER JOIN AspNetUsers on AspNetUsers.Id = Orders.UserId) where AspNetUsers.Id = @UserId", new { UserId = userId }).ToList();
            }
        }

        public List<GetAllBookOrderFullViewModel> GetAllBig()
        {
            using (dbConnection)
            {
                try
                {
                    return dbConnection.Query<GetAllBookOrderFullViewModel>("SELECT [Orders].Id, [Orders].Cost, BookOrder.BookId, Books.Name, Books.Cost FROM [Orders], BookOrder,Books WHERE BookOrder.BookId=Books.id AND [Orders].Id = BookOrder.OrderId").ToList();

                }
                catch (Exception e)
                {
                }
                return dbConnection.Query<GetAllBookOrderFullViewModel>("SELECT [Orders].Id, [Orders].Cost, BookOrder.BookId, Books.Name, Books.Cost FROM [Orders], BookOrder,Books WHERE BookOrder.BookId=Books.id AND [Orders].Id = BookOrder.OrderId").ToList();

            }
        }

        public Order Get(int id)
        {
            using (dbConnection)
            {
                return dbConnection.Query<Order>("SELECT * FROM [Orders] WHERE Id = @Id", new { Id = id }).FirstOrDefault();
            }
        }

        public long Create(Order order)
        {
            using (dbConnection)
            {
                order.CreationDate = DateTime.Now;

                var sqlQuery = "INSERT INTO [Orders] (CreationDate, UserId, Cost) VALUES(@CreationDate, @UserId, @Cost); SELECT CAST(SCOPE_IDENTITY() as int)";
                int? orderId = dbConnection.Query<int>(sqlQuery, order).FirstOrDefault();
                return orderId.Value;
            }
        }

        public void Update(Order order)
        {
            using (dbConnection)
            {
                order.CreationDate = DateTime.Now;

                var sqlQuery = "UPDATE [Orders] SET CreationDate=@CreationDate, UserId=@UserId, Cost = @Cost WHERE Id = @Id";
                dbConnection.Execute(sqlQuery, order);
            }
        }

        public void Delete(int id)
        {
            using (dbConnection)
            {
                var sqlQuery = "DELETE FROM [Order] WHERE Id = @id";
                dbConnection.Execute(sqlQuery, new { id });
            }
        }
    }
}