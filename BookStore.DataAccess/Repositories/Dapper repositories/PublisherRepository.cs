﻿using BookStore.DataAccess.Entities;
using BookStore.DataAccess.RepositoryInterfaces;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;


namespace BookStore.DataAccess.DapperRepositories
{
    public class PublisherRepository : BaseRepository<Publisher>, IPublisherRepository
    {
        public PublisherRepository(string conn)
        {
            connectionString = conn;
        }

        public List<Publisher> GetAll()
        {
            using (dbConnection)
            {
                return dbConnection.Query<Publisher>("SELECT * FROM Publishers").ToList();
            }
        }

        public Publisher Get(int id)
        {
            using (dbConnection)
            {
                return dbConnection.Query<Publisher>("SELECT * FROM Publishers WHERE Id = @id", new { id }).FirstOrDefault();
            }
        }

        public long Create(Publisher publisher)
        {
            using (dbConnection)
            {
                publisher.CreationDate = DateTime.Now;

                var sqlQuery = "INSERT INTO Publishers (CreationDate, Name) VALUES(@CreationDate, @Name); SELECT CAST(SCOPE_IDENTITY() as int)";
                int? publisherId = dbConnection.Query<int>(sqlQuery, publisher).FirstOrDefault();
                return publisherId.Value;
            }
        }

        public void Update(Publisher publisher)
        {
            using (dbConnection)
            {
                publisher.CreationDate = DateTime.Now;

                var sqlQuery = "UPDATE Publishers SET CreationDate=@CreationDate, Name = @Name WHERE Id = @Id";
                dbConnection.Execute(sqlQuery, publisher);
            }
        }

        public void Delete(int id)
        {
            using (dbConnection)
            {
                var sqlQuery = "DELETE FROM Publishers WHERE Id = @id";
                dbConnection.Execute(sqlQuery, new { id });
            }
        }
    }
}