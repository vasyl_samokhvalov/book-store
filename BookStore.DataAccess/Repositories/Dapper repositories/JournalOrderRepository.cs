﻿using BookStore.DataAccess.Entities;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using BookStore.DataAccess.RepositoryInterfaces;


namespace BookStore.DataAccess.DapperRepositories
{
    public class JournalOrderRepository : BaseRepository<JournalOrder>, IJournalOrderRepository
    {
        public JournalOrderRepository(string conn)
        {
            connectionString = conn;
        }

        public List<JournalOrder> GetAll()
        {
            using (dbConnection)
            {
                return dbConnection.Query<JournalOrder>("SELECT * FROM JournalOrder").ToList();
            }
        }

        public JournalOrder Get(int id)
        {
            using (dbConnection)
            {
                return dbConnection.Query<JournalOrder>("SELECT * FROM JournalOrder WHERE Id = @id", new { id }).FirstOrDefault();
            }
        }

        public long Create(JournalOrder journalOrder)
        {
            using (dbConnection)
            {
                journalOrder.CreationDate = DateTime.Now;

                var sqlQuery = "INSERT INTO JournalOrder (CreationDate, JournalId, OrderId) VALUES(@CreationDate, @JournalId, @OrderId); SELECT CAST(SCOPE_IDENTITY() as int)";
                int? journalOrderId = dbConnection.Query<int>(sqlQuery, journalOrder).FirstOrDefault();
                return journalOrderId.Value;
            }
        }

        public void Update(JournalOrder journalOrder)
        {
            using (dbConnection)
            {
                journalOrder.CreationDate = DateTime.Now;

                var sqlQuery = "UPDATE JournalOrder SET CreationDate=@CreationDate, JournalId=@JournalId, OrderId=@OrderId WHERE Id = @Id";
                dbConnection.Execute(sqlQuery, journalOrder);
            }
        }

        public void Delete(int id)
        {
            using (dbConnection)
            {
                var sqlQuery = "DELETE FROM JournalOrder WHERE Id = @id";
                dbConnection.Execute(sqlQuery, new { id });
            }
        }
    }
}