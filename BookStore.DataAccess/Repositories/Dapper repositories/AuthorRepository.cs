﻿using BookStore.DataAccess.Entities;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using BookStore.DataAccess.RepositoryInterfaces;

namespace BookStore.DataAccess.DapperRepositories
{
    public class AuthorRepository : BaseRepository<Author>, IAuthorRepository
    {
        public AuthorRepository(string conn)
        {
            connectionString = conn;
        }
        public List<Author> GetAll()
        {
            using (dbConnection)
            {
                return dbConnection.Query<Author>("SELECT * FROM Authors").ToList();
            }
        }

        public Author Get(int id)
        {
            using (dbConnection)
            {
                return dbConnection.Query<Author>("SELECT * FROM Authors WHERE Id = @id", new { id }).FirstOrDefault();
            }
        }

        public long Create(Author author)
        {
            using (dbConnection)
            {
                author.CreationDate = DateTime.Now;

                var sqlQuery = "INSERT INTO Authors (CreationDate, Name, Surname) VALUES(@CreationDate, @Name, @Surname); SELECT CAST(SCOPE_IDENTITY() as int)";
                int? authorId = dbConnection.Query<int>(sqlQuery, author).FirstOrDefault();
                return authorId.Value;
            }
        }

        public void Update(Author author)
        {
            using (dbConnection)
            {
                author.CreationDate = DateTime.Now;

                var sqlQuery = "UPDATE Authors SET CreationDate=@CreationDate, Name = @Name, Surname = @Surname WHERE Id = @Id";
                dbConnection.Execute(sqlQuery, author);
            }
        }

        public void Delete(int id)
        {
            using (dbConnection)
            {
                var sqlQuery = "DELETE FROM Authors WHERE Id = @id";
                dbConnection.Execute(sqlQuery, new { id });
            }
        }
    }
}