﻿using BookStore.DataAccess.Entities;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using BookStore.DataAccess.RepositoryInterfaces;


namespace BookStore.DataAccess.DapperRepositories
{
    public class BookAuthorRepository : BaseRepository<BookAuthor>, IBookAuthorRepository
    {
        public BookAuthorRepository(string conn)
        {
            connectionString = conn;
        }

        public List<BookAuthor> GetAll()
        {
            using (dbConnection)
            {
                return dbConnection.Query<BookAuthor>("SELECT * FROM BookAuthor").ToList();
            }
        }

        public List<BookAuthor> GetAll(int id)
        {
            using (dbConnection)
            {
                return dbConnection.Query<BookAuthor>("SELECT * FROM BookAuthor WHERE BookId = @id", new { id }).ToList();
            }
        }

        public BookAuthor Get(int id)
        {
            using (dbConnection)
            {
                return dbConnection.Query<BookAuthor>("SELECT * FROM BookAuthor WHERE Id = @id", new { id }).FirstOrDefault();
            }
        }

        public long Create(BookAuthor bookAuthor)
        {
            using (dbConnection)
            {
                bookAuthor.CreationDate = DateTime.Now;

                var sqlQuery = "INSERT INTO BookAuthor (CreationDate, BookId, AuthorId) VALUES(@CreationDate, @BookId, @AuthorId); SELECT CAST(SCOPE_IDENTITY() as int)";
                int? bookAuthorId = dbConnection.Query<int>(sqlQuery, bookAuthor).FirstOrDefault();
                return bookAuthorId.Value;
            }
        }

        public void Update(BookAuthor bookAuthor)
        {
            using (dbConnection)
            {
                bookAuthor.CreationDate = DateTime.Now;

                var sqlQuery = "UPDATE BookAuthor SET CreationDate=@CreationDate, BookId=@BookId, AuthorId=@AuthorId WHERE Id = @Id";
                dbConnection.Execute(sqlQuery, bookAuthor);
            }
        }

        public void Delete(int id)
        {
            using (dbConnection)
            {
                var sqlQuery = "DELETE FROM BookAuthor WHERE Id = @id";
                dbConnection.Execute(sqlQuery, new { id });
            }
        }
    }
}