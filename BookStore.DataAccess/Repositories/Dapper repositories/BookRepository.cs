﻿using BookStore.DataAccess.Entities;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using BookStore.DataAccess.RepositoryInterfaces;
using System.Threading.Tasks;

namespace BookStore.DataAccess.DapperRepositories
{
    public class BookRepository : BaseRepository<Book>, IBookRepository
    {
        public BookRepository(string conn)
        {
            connectionString = conn;
        }
        public async Task<List<Book>> GetAll()
        {
            using (dbConnection)
            {
                var result = await dbConnection.QueryAsync<Book>("SELECT * FROM Books");
                List<Book> book = result.ToList();
                return book;
                //return dbConnection.Query<Book>("SELECT * FROM Books").ToList();
            }
        }

        public List<Book> GetBasket(List<int> id)
        {
            using (dbConnection)
            {
                return dbConnection.Query<Book>("SELECT * FROM Books WHERE Id IN (@id)", new { id }).ToList();
            }
        }

        public async Task<Book> Get(int id)
        {
            using (dbConnection)
            {
                return dbConnection.Query<Book>("SELECT * FROM Books WHERE Id = @id", new { id }).FirstOrDefault();
            }
        }

        public async Task<int> Create(Book book)
        {
            using (dbConnection)
            {
                //var sqlQuery = "INSERT INTO Books (Name, Age) VALUES(@Name, @Age)";
                //return dbConnection.Insert("INSERT INTO Books (Name, Cost, Publisher, Year) VALUES(@Name, @Cost, @Publisher, @Year)");
                book.CreationDate = DateTime.Now;
                var sqlQuery = "INSERT INTO Books (CreationDate, Name, Cost, Publisher, Year) VALUES (@CreationDate, @Name, @Cost, @Publisher, @Year); SELECT CAST(SCOPE_IDENTITY() as int)";
                int? userId = dbConnection.Query<int>(sqlQuery, book).FirstOrDefault();
                return userId.Value;
            }
        }

        public void Update(Book book)
        {
            using (dbConnection)
            {
                book.CreationDate = DateTime.Now;
                var sqlQuery = "UPDATE Books SET CreationDate=@CreationDate, Name = @Name, Cost = @Cost, Publisher = @Publisher, Year = @Year WHERE Id = @Id";
                dbConnection.Execute(sqlQuery, book);
            }
        }

        public void Delete(int id)
        {
            using (dbConnection)
            {
                var sqlQuery = "DELETE FROM Books WHERE Id = @id";
                dbConnection.Execute(sqlQuery, new { id });
            }
        }
    }
}