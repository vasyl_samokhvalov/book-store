﻿using BookStore.DataAccess.Entities;
using System;
using System.Data.SqlClient;
using BookStore.DataAccess.RepositoryInterfaces;


namespace BookStore.DataAccess.DapperRepositories
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        public static string connectionString = "Server=DESKTOP-VVGE7A7;Database=BookStoreDB;Trusted_Connection=True;";

        public SqlConnection dbConnection = new SqlConnection("Server=DESKTOP-VVGE7A7;Database=BookStoreDB;Trusted_Connection=True;");

        long IBaseRepository<T>.Create(T entity) => throw new NotImplementedException();

        void IBaseRepository<T>.Update(T entity) => throw new NotImplementedException();

        void IBaseRepository<T>.Delete(int id) => throw new NotImplementedException();

        T IBaseRepository<T>.Get(int id)
        {
            return null;
        }
    }
}
