﻿using BookStore.DataAccess.Entities;
using BookStore.DataAccess.EntityRepositories;
using BookStore.DataAccess.RepositoryInterfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Entity_repositories
{
    public class BookRepository : BaseRepository<Book>, IBookRepository
    {
        public async Task<int> Create(Book book)
        {
            await dbContext.Books.AddAsync(book);
            await dbContext.SaveChangesAsync();
            return await dbContext.Books.CountAsync();
        }

        public void Delete(int id)
        {
            using (dbContext)
            {
                throw new NotImplementedException();
            }
        }

        public async Task<Book> Get(int id)
        {
            using (dbContext)
            {
                return await dbContext.Books.FirstOrDefaultAsync(b => b.Id == id);
            }
        }

        public async Task<List<Book>> GetAll()
        {
            return await dbContext.Books.ToListAsync<Book>();
        }

        public List<Book> GetBasket(List<int> id)
        {
            throw new NotImplementedException();
        }

        public void Update(Book book)
        {
            throw new NotImplementedException();
        }
    }
}
