﻿namespace BookStore.DataAccess.Entities
{
    public class Genre : BaseEntity
    {
        public string Name { get; set; }
    }
}
