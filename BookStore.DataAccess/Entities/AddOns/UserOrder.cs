﻿namespace BookStore.DataAccess.Entities
{
    public class UserOrder : BaseEntity
    {
        public string UserId { get; set; }
        public int Cost { get; set; }
        public string Name { get; set; }
    }
}
