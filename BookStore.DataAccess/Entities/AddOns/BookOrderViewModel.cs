﻿using System.Collections.Generic;

namespace BookStore.DataAccess.Entities.AddOns
{
    public class GetAllBookOrderFullView
    {
        public List<GetAllBookOrderFullViewModel> bookOrders { get; set; }

        public GetAllBookOrderFullView()
        {
            bookOrders = new List<GetAllBookOrderFullViewModel>();
        }
    }

    public class GetAllBookOrderFullViewModel
    {
        public int OrderId { get; set; }
        public int OrderCost { get; set; }
        public int BookId { get; set; }
        public string BookName { get; set; }
        public int BookCost { get; set; }
    }
}
