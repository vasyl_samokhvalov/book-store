﻿namespace BookStore.DataAccess.Entities
{
    public class Order : BaseEntity
    {
        public string UserId { get; set; }
        public int Cost { get; set; }
    }
}
