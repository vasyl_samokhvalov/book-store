﻿namespace BookStore.DataAccess.Entities
{
    public class JournalOrder : BaseEntity
    {
        public int JounalId { get; set; }
        public Journal Journal { get; set; }
        public int OrderId { get; set; }
        public Order Order { get; set; }
    }
}
