﻿namespace BookStore.DataAccess.Entities
{
    public class BookJournal : BaseEntity
    {
        public int BookId { get; set; }
        public Book Book { get; set; }
        public int JournalId { get; set; }
        public Journal Journal { get; set; }
    }

}
