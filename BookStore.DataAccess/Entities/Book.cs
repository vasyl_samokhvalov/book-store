﻿namespace BookStore.DataAccess.Entities
{
    public class Book : BaseEntity
    {
        public string Name { get; set; }
        public int Cost { get; set; }
        public int Publisher { get; set; }
        public int Year { get; set; }
    }
}
