﻿namespace BookStore.DataAccess.Entities
{
    public class Journal : BaseEntity
    {
        public string Name { get; set; }
        public int Cost { get; set; }
    }
}
